# SCRIPTER #

Scripter is an app for reading and editing movie scripts. I started this project to study the application
development in Windows 10 Universal Windows Platform environment. My target is to get it released in Windows
Store as a free app at some point. Since I have some other things in my life as well I haven't planned any
concrete schedule when the app will be ready. Also the actual feature set is still pretty much open.

But there is one thing I know for sure already. The app name 'Scripter' will not be the final name of the
app. I will rename it at some point.

![Screenshot of the app](https://bitbucket.org/tomi_lamminsaari/scripter/raw/master/screenshots/scripter_shot1.jpg)