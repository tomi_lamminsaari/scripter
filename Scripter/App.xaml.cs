﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Scripter.DataModel;
using System.Diagnostics;
using Scripter.View;
using Scripter.Resources;

namespace Scripter
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application, IAppInterface
    {

        private MovieScriptManager mMovieScriptManager = new MovieScriptManager();
        private DocumentManager mDocManager;
        private PageStateStorage mPageStorage;
        private string mActiveScriptId;
        private CustomResourceHelper mResourceHelper = new CustomResourceHelper();


        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            Microsoft.ApplicationInsights.WindowsAppInitializer.InitializeAsync(
                Microsoft.ApplicationInsights.WindowsCollectors.Metadata |
                Microsoft.ApplicationInsights.WindowsCollectors.Session);
            this.InitializeComponent();
            this.Suspending += OnSuspending;


            // Prepare the script document storage.
            mDocManager = new DocumentManager();
            mActiveScriptId = "a0a9a8a7";

            // Create the storage where to store the UI state of pages.
            mPageStorage = new PageStateStorage();
        }

        private void App_BackRequested(object sender, BackRequestedEventArgs e)
        {
            Debug.WriteLine("Back pressed!");
            Frame frame = Window.Current.Content as Frame;
            if (frame.CanGoBack)
            {
                e.Handled = true;
                frame.GoBack();
            }
        }


        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
//                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif
            Frame rootFrame = Window.Current.Content as Frame;

            // Create the database instance.
            

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;

            }

            if (rootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                rootFrame.Navigate(typeof(HomeView), e.Arguments);
            }
            // Ensure the current window is active
            Window.Current.Activate();

            // Register the back button event listener.
            SystemNavigationManager.GetForCurrentView().BackRequested += App_BackRequested;

        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            Debug.WriteLine("App.OnSuspending()");
            // Unload the custom resource data from memory.
            mResourceHelper.Clear();

            // Release other resources.
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }



        DocumentManager IAppInterface.GetDocumentManager()
        {
            return mDocManager;
        }

        MovieScriptManager IAppInterface.GetMovieScriptManager()
        {
            return mMovieScriptManager;
        }

        public void HideBackButton()
        {
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = 
                AppViewBackButtonVisibility.Collapsed;
        }

        public void ShowBackButton()
        {
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
                AppViewBackButtonVisibility.Visible;
        }


        public PageStateStorage GetPageStateStorage()
        {
            return mPageStorage;
        }


        public string GetActiveScriptId()
        {
            return mActiveScriptId;
        }


        /// <summary>
        /// Returns the current application object typecasted to App
        /// </summary>
        /// <returns>Reference to App instance.</returns>
        public static App GetCurrentApp()
        {
            return (App)Application.Current;
        }


        /// <summary>
        /// Returns the custom resource file manager instance.
        /// </summary>
        /// <returns>Custom resource file manager object.</returns>
        public CustomResourceHelper GetCustomResources()
        {
            return mResourceHelper;
        }


        /// <summary>
        /// Returns the URI of the resource file that contains layout properties
        /// like margins and text colors.
        /// </summary>
        /// <returns>String containing the uri of the custom layout resource file.</returns>
        public string GetLayoutParamsResourceUri()
        {
            return "ms-appx:///Resources/LayoutParamsWin10.xaml";
        }

    }
}
