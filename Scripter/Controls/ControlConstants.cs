﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.Controls
{
    public static class ControlConstants
    {
        /// <summary>
        /// Constant that is used with ISceneElementEditor's SetEditorFocused()-method. Instructs to
        /// set the focus to the first line.
        /// </summary>
        public const int FocusLineFirst = -1;

        /// <summary>
        /// Constant that is used with ISceneElementEditor's SetEditorFocused()-method. Instructs to
        /// set the focus to the last line.
        /// </summary>
        public const int FocusLineLast = -2;
    }
}
