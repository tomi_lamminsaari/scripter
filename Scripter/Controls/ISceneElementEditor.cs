﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.Controls
{
    /// <summary>
    /// Interface the scene element controls implement. This interface provides
    /// access to the scene element's data.
    /// </summary>
    public interface ISceneElementEditor
    {
        /// <summary>
        /// Returns the text content of this control as a string.
        /// </summary>
        /// <returns>Text content of this control.</returns>
        string GetContent();

        /// <summary>
        /// Sets the text content to this control as string.
        /// </summary>
        /// <param name="content">The text content to be set.</param>
        void SetContent(string content);

        /// <summary>
        /// Sets the element's ID number. Should be the index number of the element
        /// in layout.
        /// </summary>
        /// <param name="id">ID of this scene element.</param>
        void SetId(int id);

        /// <summary>
        /// Returns the scene element's ID number.
        /// </summary>
        /// <returns>ID of this scene element.</returns>
        int GetId();

        /// <summary>
        /// Returns the type of this scene element.
        /// </summary>
        /// <returns>Type of this scene element.</returns>
        DataModel.SceneElement.ElemType GetSceneElementType();

        /// <summary>
        /// Activates the input focus to this editor.
        /// <param name="focusHint">Hint regarding where in the editor the input cursor
        /// will be set.</param>
        /// </summary>
        void SetEditorFocused(int focusHint);
    }
}
