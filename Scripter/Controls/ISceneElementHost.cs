﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Scripter.Controls
{
    /// <summary>
    /// Defines the possible reasons why scene element editing may end.
    /// </summary>
    public enum EditingEndReason
    {
        EditingEndsEnterKey,
        EditingEndsLostFocus,
        EditingEndsFocusPrevious,
        EditingEndsFocusNext
        
    }

    /// <summary>
    /// ISceneElementHost is the interface the scene element controls use to communicate
    /// with the hosting view.
    /// </summary>
    public interface ISceneElementHost
    {
        /// <summary>
        /// Called by the scene element editor control when editing ends. Possible reasons could be
        /// events such as user pressing the enter key.
        /// </summary>
        /// <param name="editedControl">The related UI control.</param>
        /// <param name="endReason">End reason that caused the end of the editing.</param>
        void SceneElementEditingEnded(ISceneElementEditor editedControl, EditingEndReason endReason);
    }
}
