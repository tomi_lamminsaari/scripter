﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.Controls
{

    public enum SceneInsertionPos { BeforeRefScene, AfterRefScene };


    /// <summary>
    /// IScenePanelObserver is the utility interface the ScenePanelControl uses
    /// to ask the hosting control to create new scene to the script.
    /// </summary>
    public interface IScenePanelObserver
    {
        /// <summary>
        /// Function that creates new scene to currently opened script.
        /// </summary>
        /// <param name="refScene">The reference scene. The new scene will be
        /// inserted either before or after this scene.</param>
        /// <param name="scenePos">Defines whether the new scene will be
        /// created before or after the given reference scene.</param>
        void CreateNewScene(DataModel.Scene refScene, SceneInsertionPos scenePos);

        /// <summary>
        /// This gets called when user wants to start editing the scene. The
        /// implementation of this function is supposed to open the scene
        /// editing view.
        /// </summary>
        /// <param name="scene">The scene object the user wants to start
        /// editing.</param>
        void OpenSceneEditor(DataModel.Scene scene);
    }
}
