﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Scripter.Controls
{
    public sealed partial class SceneElementAction : UserControl, ISceneElementEditor
    {
        #region Members

        private ISceneElementHost mHost;
        private int mId = 0;
        private int mCursorPosOnLastKeyUp = -1;

        #endregion

        public SceneElementAction()
        {
            this.InitializeComponent();
            this.KeyUp += SceneElementAction_KeyUp;

            this.TextContent.KeyUp += SceneElementAction_TextBox_KeyUp;
            this.TextContent.KeyDown += SceneElementAction_TextBox_KeyDown;
        }

        private void SceneElementAction_TextBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            int cl = this.TextContent.SelectionStart;
            Debug.WriteLine(String.Format("Cur pos DOWN: {0}", cl));
            
        }

        private void SceneElementAction_TextBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            int cl = this.TextContent.SelectionStart;
            Debug.WriteLine(String.Format("Cur pos UP: {0}", cl));
        }

        private void SceneElementAction_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (mHost == null) {
                return;
            }
            Debug.WriteLine("Parent key UP: {0}", this.TextContent.SelectionStart);
            int currentCursorPos = this.TextContent.SelectionStart;

            if (e.Key.Equals(VirtualKey.Enter)) {
                mHost.SceneElementEditingEnded(this, EditingEndReason.EditingEndsEnterKey);

            } else if (e.Key.Equals(VirtualKey.Up)) {
                if (mCursorPosOnLastKeyUp == currentCursorPos) {
                    // Cursor has not moved since last up arrow key up event. Move focus to previous
                    // scene element.
                    mHost.SceneElementEditingEnded(this, EditingEndReason.EditingEndsFocusPrevious);
                } else {
                    mCursorPosOnLastKeyUp = currentCursorPos;

                }
                

            } else if (e.Key.Equals(VirtualKey.Down)) {
                if (mCursorPosOnLastKeyUp == currentCursorPos) {
                    // Cursor has not moved since last down arrow key up event. Move focus to next
                    // scene element.
                    mHost.SceneElementEditingEnded(this, EditingEndReason.EditingEndsFocusNext);
                } else {
                    mCursorPosOnLastKeyUp = currentCursorPos;
                }
                

            } else {
                // Not arrow up or arrow down. Clear cursor position variable.
                mCursorPosOnLastKeyUp = -1;
            }
        }

        /// <summary>
        /// Sets the scene element host interface.
        /// </summary>
        /// <param name="host">Reference to the scene element host instance.</param>
        public void SetSceneElementHost(ISceneElementHost host)
        {
            mHost = host;
        }

        public void SetContent(string actionText)
        {
            this.TextContent.Text = actionText;
        }

        public string GetContent()
        {
            return this.TextContent.Text;
        }

        public DataModel.SceneElement.ElemType GetSceneElementType()
        {
            return DataModel.SceneElement.ElemType.SETypeAction;
        }

        public void SetId(int id)
        {
            mId = id;
        }

        public int GetId()
        {
            return mId;
        }

        public void SetEditorFocused(int focusHint)
        {
            if (focusHint == ControlConstants.FocusLineFirst) {
                this.TextContent.Select(0, 0);
            } else {
                this.TextContent.Select(this.TextContent.Text.Length - 1, 0);
            };

            this.Focus(FocusState.Keyboard);
        }
    }
}
