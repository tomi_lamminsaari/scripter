﻿using Scripter.DataModel;
using Scripter.Resources;
using Scripter.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Scripter.Controls
{
    /// <summary>
    /// A user control that can be used for editing the scene elements.
    /// </summary>
    public sealed partial class SceneElementEditor : UserControl
    {

        /// <summary>
        /// An interface that is supposed to give support for integrating this
        /// editor to the actual XAML page. Typically the custom Page object
        /// implements this interface.
        /// </summary>
        public interface ISceneElementHelper
        {
            void AddSceneElementEditor(SceneElementEditor refElement, bool addAbove);
            void RemoveSceneElementEditor(SceneElementEditor me);
        }


        /// <summary>
        /// A class that carries the information of the editor type. Needed when
        /// processing the scene element editors to create or update a scene.
        /// </summary>
        private class SceneElemTag
        {
            public DataModel.SceneElement.ElemType mType = SceneElement.ElemType.SETypeUnspecified;

            public SceneElemTag(SceneElement.ElemType et)
            {
                mType = et;
            }
        }



        private SceneElement mElement;
        private bool mIsFirstItem = false;
        private ISceneElementHelper mHelper = null;
        private ScriptTextRecognizer mRecognizer = new ScriptTextRecognizer();



        public SceneElementEditor()
        {
            this.InitializeComponent();

            // Add event listeners
            this.TextContent.LostFocus += TextContent_LostFocus;

            this.EditorRoot.PointerEntered += EditorRoot_PointerEntered;
            this.EditorRoot.PointerExited += EditorRoot_PointerExited;

            this.TextContent.Tag = new SceneElemTag(SceneElement.ElemType.SETypeUnspecified);
        }

        private void EditorRoot_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "HiddenButtonsState", true);
        }

        private void EditorRoot_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "ShownButtonsState", true);
        }


        /// <summary>
        /// This function should be called when preparing this editor component to
        /// edit certain scene element. This will take the text contents from given
        /// scene element and format the appearance correctly.
        /// </summary>
        /// <param name="elem">Reference to the scene element that will be edited.</param>
        public void PrepareEditor(SceneElement elem)
        {
            mElement = elem;
            if (mElement == null)
            {
                // Element is null. Just prepare empty scene editor with unspecified
                // element type.
                this.TextContent.Text = "";
                this.TextContent.Tag = new SceneElemTag(SceneElement.ElemType.SETypeUnspecified);
                FormatForUnspecifiedType();
            }
            else
            {
                // When preparing this editor component with scene element, we
                // copy the text from scene element to the text editor. The
                // formatting functions then set the margins and colors
                // to match the style for such element type.
                this.TextContent.Text = mElement.GetAsString();
                this.TextContent.Tag = new SceneElemTag(elem.GetElementType());
                FormatForTypeDefinedByTag(this.TextContent.Tag as SceneElemTag);
            }
        }


        /// <summary>
        /// Stores the content from TextBox editor to the actual SceneElement
        /// instance. If this editor component don't have such SceneElement
        /// instance yet, this function will create that SceneElement first.
        /// </summary>
        public void UpdateEditorContentToSceneElement()
        {
            /*
            if (mElement == null)
            {
                // No SceneElement instance. This probably means that user has
                // recently created this in editor and the actual Scene instance
                // don't have this element yet. We create the scene element here.
                SceneElemTag tag = this.TextContent.Tag as SceneElemTag;
                mElement = SceneElementFactory.CreateElement(tag.mType);
            }

            if (mElement as SceneElementAction != null)
            {
                var actionElem = mElement as SceneAction;
                actionElem.ActionText = this.TextContent.Text;

            }
            else if (mElement as Dialoque != null)
            {

                var dialogElem = mElement as Dialoque;
                dialogElem.ClearDialogLines();
                string[] lines = StringParseUtils.SplitLines(this.TextContent.Text);
                if (lines.Length > 0)
                {
                    // Iterate all the lines through and add them to the Dialoque-object.
                    bool personNameSetAlready = false;
                    foreach (var l in lines)
                    {
                        if (StringParseUtils.OnlyUpperCaseCharacters(l) && personNameSetAlready == false)
                        {
                            // The first line that contains only upper case letters is treated as
                            // person name.
                            dialogElem.PersonName = l;
                            personNameSetAlready = true;
                        }
                        else
                        {
                            dialogElem.AddDialogLine(l);
                        }
                    }
                }
                
            }
            */
        }


        /// <summary>
        /// User must call this function to set the access to the utility interface
        /// that provides implementations for the needed functionalities.
        /// </summary>
        /// <param name="helper">The implementation of the utility interface.</param>
        public void SetSceneEditorHelper(ISceneElementHelper helper)
        {
            mHelper = helper;
        }


        /// <summary>
        /// Informs this editor control if it is the very first scene element editor
        /// in the view. Some buttons look different for the first control.
        /// </summary>
        public bool IsFirstItem
        {
            get { return mIsFirstItem; }
            set
            {
                mIsFirstItem = value;
                if (mIsFirstItem)
                {
                    this.AddAboveButton.Visibility = Visibility.Visible;
                }
                else
                {
                    this.AddAboveButton.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// Returns the text from the textbox control.
        /// </summary>
        /// <returns>The text edited by the user.</returns>
        public string GetEditedText()
        {
            return this.TextContent.Text;
        }


        /// <summary>
        /// Returns the SceneElement instance that relates to this editor.
        /// </summary>
        /// <returns></returns>
        public SceneElement GetSceneElement()
        {
            return mElement;
        }


        /// <summary>
        /// Tells if this scene element editor component is having the input focus.
        /// </summary>
        /// <returns>Returns true if this editor has keyboard input focus.</returns>
        public bool IsHavingInputFocus()
        {
            Debug.WriteLine("IsHavingFocus(): " + this.TextContent.FocusState.ToString());
            return this.TextContent.FocusState != FocusState.Unfocused;
        }


        /// <summary>
        /// Makes this scene element editor to capture the input focus.
        /// </summary>
        public void CaptureInputFocus()
        {
            this.TextContent.Focus(FocusState.Keyboard);
        }


        private void TextContent_LostFocus(object sender, RoutedEventArgs e)
        {
            // When textbox looses focus, we parse the content and try to set the
            // type to correct one.
            TextBox tb = (TextBox)sender;
            SceneElemTag tag = tb.Tag as SceneElemTag;
            if (tag != null)
            {
                if (tag.mType == SceneElement.ElemType.SETypeUnspecified)
                {
                    // No type specified yet. Detect element type from the content.
                    tag.mType = mRecognizer.DetectSceneElemType(tb.Text);
                    Debug.WriteLine("Type detected! " + tag.mType.ToString());
                }

                // At this point we should know the type of the scene element the
                // editor contains. It is possible that type is still unspecified
                // but then we just format it for unspecified.
                FormatForTypeDefinedByTag(tag);

                // Check if there are parse errors. If there are, we show red
                // exclamation mark to notify the user that something is wrong.
                UpdateParseErrorIndicator();
            }
            else
            {
                Debug.WriteLine("TextContent_LostFocus(): Tag is null!");
            }
        }

        private void UpdateParseErrorIndicator()
        {
            SceneElemTag tag = (SceneElemTag)this.TextContent.Tag;
            bool noErrors = mRecognizer.ParseTextBoxContents(this.TextContent.Text, tag.mType);
            if (noErrors)
            {
                ParseErrorIndicator.Visibility = Visibility.Collapsed;
            }
            else
            {
                ParseErrorIndicator.Visibility = Visibility.Visible;
            }
        }

        private void FormatForTypeDefinedByTag(SceneElemTag tag)
        {
            if (tag == null)
            {
                FormatForUnspecifiedType();
            }
            else
            {
                switch (tag.mType)
                {
                    case SceneElement.ElemType.SETypeAction:
                        FormatForAction();
                        break;
                    case SceneElement.ElemType.SETypeDialog:
                        FormatForDialoque();
                        break;
                    default:
                        FormatForUnspecifiedType();
                        break;
                }
            }
        }

        private void FormatForUnspecifiedType()
        {
            CustomResourceHelper resHelper = App.GetCurrentApp().GetCustomResources();
            string resUri = App.GetCurrentApp().GetLayoutParamsResourceUri();

            this.TextContent.Style = resHelper.GetStyle(resUri, "SceneElementEditorStyle");
            this.TextContent.Margin = new Thickness(30.0, 5.0, 30.0, 5.0);
            this.TextContent.AcceptsReturn = true;
            this.TextContent.TextAlignment = TextAlignment.Left;
            this.TextContent.Tag = new SceneElemTag(SceneElement.ElemType.SETypeUnspecified);
        }

        private void FormatForAction()
        {
            CustomResourceHelper resHelper = App.GetCurrentApp().GetCustomResources();
            string resUri = App.GetCurrentApp().GetLayoutParamsResourceUri();

            this.TextContent.Style = resHelper.GetStyle(resUri, "SceneElementEditorStyle");
            this.TextContent.Margin = resHelper.GetMarginResource(resUri, "SceneActionEditMargin");
            this.TextContent.AcceptsReturn = true;
            this.TextContent.TextAlignment = TextAlignment.Left;
            this.TextContent.Tag = new SceneElemTag(SceneElement.ElemType.SETypeAction);

            // Update the appearance of type selection buttons
            this.ChangeTypeAction.Style = resHelper.GetStyle(resUri, "SelectedSceneTypeButtonStyle");
            this.ChangeTypeDialoque.Style = resHelper.GetStyle(resUri, "UnselectedSceneTypeButtonStyle");
            this.ChangeTypeShot.Style = resHelper.GetStyle(resUri, "UnselectedSceneTypeButtonStyle");
        }

        private void FormatForDialoque()
        {
            CustomResourceHelper resHelper = App.GetCurrentApp().GetCustomResources();
            string resUri = App.GetCurrentApp().GetLayoutParamsResourceUri();

            // Prepare the textbox styles
            this.TextContent.Style = resHelper.GetStyle(resUri, "SceneElementEditorStyle");
            this.TextContent.Margin = resHelper.GetMarginResource(resUri, "SceneDialoqueEditMargin");
            this.TextContent.AcceptsReturn = true;
            this.TextContent.TextAlignment = TextAlignment.Center;
            this.TextContent.Tag = new SceneElemTag(SceneElement.ElemType.SETypeDialog);

            // Update the appearance of type selection buttons
            this.ChangeTypeAction.Style = resHelper.GetStyle(resUri, "UnselectedSceneTypeButtonStyle");
            this.ChangeTypeDialoque.Style = resHelper.GetStyle(resUri, "SelectedSceneTypeButtonStyle");
            this.ChangeTypeShot.Style = resHelper.GetStyle(resUri, "UnselectedSceneTypeButtonStyle");
        }

        private void AddAboveButtonPressed(object sender, RoutedEventArgs e)
        {
            if (mHelper != null)
            {
                mHelper.AddSceneElementEditor(this, true);
            }
        }

        private void AddBelowButtonPressed(object sender, RoutedEventArgs e)
        {
            if (mHelper != null)
            {
                mHelper.AddSceneElementEditor(this, false);
            }
        }

        private void RemoveElementButtonPressed(object sender, RoutedEventArgs e)
        {
            if (mHelper != null)
            {
                mHelper.RemoveSceneElementEditor(this);
            }
        }

        private void ChangeTypeAction_Click(object sender, RoutedEventArgs e)
        {
            FormatForAction();
            UpdateParseErrorIndicator();
        }

        private void ChangeTypeDialoque_Click(object sender, RoutedEventArgs e)
        {
            FormatForDialoque();
            UpdateParseErrorIndicator();
        }

        private void ChangeTypeShot_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
