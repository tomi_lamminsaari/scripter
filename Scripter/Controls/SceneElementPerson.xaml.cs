﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Scripter.DataModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.System;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Scripter.Controls
{
    public sealed partial class SceneElementPerson : UserControl, ISceneElementEditor
    {
        #region Members

        private ISceneElementHost mHost;
        private int mId = 0;

        #endregion

        public SceneElementPerson()
        {
            this.InitializeComponent();
            this.KeyUp += SceneElementPerson_KeyUp;
        }

        private void SceneElementPerson_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key.Equals(VirtualKey.Enter)) {
                if (mHost != null) {
                    mHost.SceneElementEditingEnded(this, EditingEndReason.EditingEndsEnterKey);
                }
            }
        }

        /// <summary>
        /// Sets the scene element host interface.
        /// </summary>
        /// <param name="host">Reference to the scene element host instance.</param>
        public void SetSceneElementHost(ISceneElementHost host)
        {
            mHost = host;
        }

        public string GetContent()
        {
            return this.TextContent.Text;
        }

        public int GetId()
        {
            return mId;
        }

        public SceneElement.ElemType GetSceneElementType()
        {
            return SceneElement.ElemType.SETypePerson;
        }

        public void SetContent(string content)
        {
            this.TextContent.Text = content;
        }

        public void SetId(int id)
        {
            mId = id;
        }

        public void SetEditorFocused(int focusHint)
        {
            if (focusHint == -1) {
                this.TextContent.Select(0, 0);
            } else {
                this.TextContent.Select(this.TextContent.Text.Length - 1, 0);
            };
            
            this.Focus(FocusState.Keyboard);
        }

     
    }
}
