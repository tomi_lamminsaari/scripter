﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Diagnostics;
using System.Windows;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Documents;
using Scripter.DataModel;
using Scripter.Resources;
using System.Windows.Input;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Scripter.Controls
{

    public sealed partial class ScenePanelControl : UserControl
    {

        public static int sCounter = 0;

        private int mIndexNum = 0;

        public ScenePanelControl()
        {
            Debug.WriteLine("ScenePanelControl!");
            this.InitializeComponent();

            mIndexNum = ++sCounter;

            Loaded += ScenePanelControl_Loaded1;
            this.DataContextChanged += ScenePanelControl_DataContextChanged;
            this.KeyDown += ScenePanelControl_KeyDown;
        }

        private void ScenePanelControl_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            Debug.WriteLine("ScenePanelControl_KeyDown!");
        }

        private void ScenePanelControl_DataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            Debug.WriteLine("DataContextChanged! ControlNum: " + mIndexNum.ToString());

            RepopulateChildElements();
        }

        private void ScenePanelControl_Loaded1(object sender, RoutedEventArgs e)
        {
            // Prepare the scene body text.
            
        }

        private void RepopulateChildElements()
        {
            this.SceneElementsLayout.Children.Clear();

            // This is The End scene. Hide editing button.
            if (GetSceneReference().TheEndFlag)
            {
                this.ShowSceneInfo.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.ShowSceneInfo.Visibility = Visibility.Visible;
            }

            CustomResourceHelper resReader = App.GetCurrentApp().GetCustomResources();
            string resUri = App.GetCurrentApp().GetLayoutParamsResourceUri();

            List<SceneElement> sceneElements = GetSceneReference().GetSceneElements();
            foreach (SceneElement elem in sceneElements)
            {
                if (elem == null)
                {
                    continue;
                }
                if (elem.GetType() == typeof(SceneElementAction))
                {
                    DataModel.SceneElementAction actionElem = elem as DataModel.SceneElementAction;
                    TextBlock actText = new TextBlock();
                    actText.Text = actionElem.GetAsString();
                    actText.TextWrapping = TextWrapping.Wrap;
                    actText.HorizontalAlignment = HorizontalAlignment.Stretch;
                    actText.Margin = resReader.GetMarginResource(resUri, "SceneActionViewerMargin");
                    actText.Style = Application.Current.Resources["SceneActionTextStyle"] as Style;
                    this.SceneElementsLayout.Children.Add(actText);
                }
                else if (elem.GetType() == typeof(DataModel.SceneElementDialog))
                {
                    DataModel.SceneElementDialog dialogElem = elem as DataModel.SceneElementDialog;

                    // Add UI element for person name.
                    var dialogCharacter = new TextBlock();
                    dialogCharacter.HorizontalAlignment = HorizontalAlignment.Center;
                    dialogCharacter.Text = dialogElem.GetAsString().ToUpper();
                    dialogCharacter.Margin = resReader.GetMarginResource(resUri, "SceneDialoqueViewerMargin");
                    dialogCharacter.Style = Application.Current.Resources["SceneDialogPersonNameStyle"] as Style;
                    this.SceneElementsLayout.Children.Add(dialogCharacter);

                    // Add the text lines that relate to this person.
                    /*
                    for (int lineIndex = 0; lineIndex < dialogElem.GetLineCount(); ++lineIndex)
                    {
                        var dialogTextBlock = new TextBlock();
                        dialogTextBlock.TextWrapping = TextWrapping.Wrap;
                        dialogTextBlock.HorizontalAlignment = HorizontalAlignment.Center;
                        dialogTextBlock.Text = dialogElem.GetLineByIndex(lineIndex);
                        if (dialogElem.IsParenthesis(lineIndex))
                        {
                            dialogTextBlock.Margin = resReader.GetMarginResource(resUri, "SceneDialoqueViewerMargin");
                            dialogTextBlock.Style = Application.Current.Resources["SceneDialogParenthesisStyle"] as Style;
                        }
                        else
                        {
                            dialogTextBlock.Margin = resReader.GetMarginResource(resUri, "SceneDialoqueViewerMargin");
                            dialogTextBlock.Style = Application.Current.Resources["SceneDialogTextStyle"] as Style;
                        }
                        this.SceneElementsLayout.Children.Add(dialogTextBlock);
                    }
                    */
                    // Dialog speaker name
#if false
                    TextBlock dialText = new TextBlock();
                    dialText.HorizontalAlignment = HorizontalAlignment.Center;
                    dialText.Text = dialogElem.PersonName.ToUpper();
                    dialText.Margin = resReader.GetMarginResource(resUri, "SceneDialoqueViewerMargin");
                    dialText.Style = Application.Current.Resources["SceneDialogPersonNameStyle"] as Style;
                    this.SceneElementsLayout.Children.Add(dialText);

                    // Dialog parenthesis
                    if (dialogElem.Parenthesis != null && dialogElem.Parenthesis.Length > 0)
                    {
                        TextBlock parenthesis = new TextBlock();
                        parenthesis.HorizontalAlignment = HorizontalAlignment.Center;
                        parenthesis.Text = dialogElem.Parenthesis;
                        parenthesis.Margin = resReader.GetMarginResource(resUri, "SceneDialoqueViewerMargin");
                        parenthesis.Style = Application.Current.Resources["SceneDialogParenthesisStyle"] as Style;
                        this.SceneElementsLayout.Children.Add(parenthesis);
                    }

                    // Actual dialog text
                    TextBlock txt = new TextBlock();
                    txt.Text = dialogElem.DialogText;
                    txt.TextWrapping = TextWrapping.Wrap;
                    txt.HorizontalAlignment = HorizontalAlignment.Center;
                    txt.Margin = resReader.GetMarginResource(resUri, "SceneDialoqueViewerBodyMargin");
                    txt.Style = Application.Current.Resources["SceneDialogTextStyle"] as Style;
                    this.SceneElementsLayout.Children.Add(txt);
#endif
                }
                else if (elem.GetType() == typeof(SceneElementShot))
                {
                    SceneElementShot shotElem = elem as SceneElementShot;

                    // Text of the SHOT element.
                    TextBlock dialText = new TextBlock();
                    dialText.HorizontalAlignment = HorizontalAlignment.Left;
                    dialText.Text = shotElem.GetAsString();
                    dialText.Style = Application.Current.Resources["SceneShotTextStyle"] as Style;
                    this.SceneElementsLayout.Children.Add(dialText);

                }
            }
        }

        private Scene GetSceneReference()
        {
            return this.DataContext as Scene;
        }

        private void ShowSceneInfo_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Show scene info pressed!");
            var sceneObj = GetSceneReference();
//            sceneObj.IncOrDecNumber(1);
            Debug.WriteLine("    Scene number: " + sceneObj.GetSceneNumber().ToString());

            var obs = (IScenePanelObserver)GetCurrentFrame().Content;
            var scene = GetSceneReference();
            obs.OpenSceneEditor(scene);
            
        }


        private void AddSceneAboveButton_Click(object sender, RoutedEventArgs e)
        {

            var frame = (Frame)Window.Current.Content;
            IScenePanelObserver obs = (IScenePanelObserver)frame.Content;
            var scene = GetSceneReference();
            obs.CreateNewScene(scene, SceneInsertionPos.BeforeRefScene);

            Debug.WriteLine("Add button pressed!");
        }

        private Frame GetCurrentFrame()
        {
            return (Frame)Window.Current.Content;
        }
    }
}
