﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.Streams;

namespace Scripter.DataModel.Converters
{
    class FountainExporter : ExporterBase
    {


        public FountainExporter()
        {

        }


        public override async Task ExportAsync(ScriptDocument doc, Windows.Storage.Streams.DataWriter writer)
        {
            await ExportScriptTitle(doc, writer);

            // Write the scene one by one.
            foreach (var scene in doc.ScenesList)
            {
                await ExportSingleScene(scene, writer);
            }
        }

        private async Task ExportScriptTitle(ScriptDocument doc, DataWriter writer)
        {
            var sb = new StringBuilder();
            sb.Append("Title: ");
            sb.Append(doc.ScriptTitle);
            sb.Append("\n");

            sb.Append("Credit: ");
            sb.Append(doc.Authors);
            sb.Append("\n");
            sb.Append("\n");

            var buffer = Windows.Security.Cryptography.CryptographicBuffer.ConvertStringToBinary(
                sb.ToString(), Windows.Security.Cryptography.BinaryStringEncoding.Utf8);

            writer.WriteBuffer(buffer);
            await writer.StoreAsync();
        }
        
        private async Task ExportSingleScene(Scene scene, DataWriter writer)
        {
            // Write the scene heading.
            var sb = new StringBuilder();
            sb.Append(scene.CombinedHeaderText);
            sb.Append("\n");
            sb.Append("\n");

            var buffer = Windows.Security.Cryptography.CryptographicBuffer.ConvertStringToBinary(
                sb.ToString(), Windows.Security.Cryptography.BinaryStringEncoding.Utf8);

            writer.WriteBuffer(buffer);
            await writer.StoreAsync();

            // Write the scene elements.
            foreach (var elem in scene.SceneElementObjects)
            {
                if (elem as SceneElementDialog != null)
                {
                    await ExportSingleDialogue(elem as SceneElementDialog, writer);
                }
                else if (elem as SceneElementAction != null)
                {
                    await ExportSingleAction(elem as SceneElementAction, writer);
                }
                else if (elem as SceneElementShot != null)
                {
                    await ExportSingleShot(elem as SceneElementShot, writer);
                }
            }

            
        }

        private async Task ExportSingleDialogue(SceneElementDialog elem, DataWriter writer)
        {
            var sb = new StringBuilder();
            
            sb.Append(elem.Text);
            
            sb.Append("\n");
            sb.Append("\n");

            var buffer = Windows.Security.Cryptography.CryptographicBuffer.ConvertStringToBinary(
                sb.ToString(), Windows.Security.Cryptography.BinaryStringEncoding.Utf8);

            writer.WriteBuffer(buffer);
            await writer.StoreAsync();
            
        }

        private async Task ExportSingleAction(SceneElementAction elem, DataWriter writer)
        {
            var sb = new StringBuilder();
            sb.Append(elem.Text);
            sb.Append("\n");
            sb.Append("\n");

            var buffer = Windows.Security.Cryptography.CryptographicBuffer.ConvertStringToBinary(
                sb.ToString(), Windows.Security.Cryptography.BinaryStringEncoding.Utf8);

            writer.WriteBuffer(buffer);
            await writer.StoreAsync();
        }

        private async Task ExportSingleShot(SceneElementShot elem, DataWriter writer)
        {
            // Todo: Some real work!
            await Task.Delay(1);
        }
    }
}
