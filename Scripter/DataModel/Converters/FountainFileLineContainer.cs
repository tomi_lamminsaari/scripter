﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.DataModel.Converters
{
    /// <summary>
    /// FountainFileLineContainer is line buffer that provides convenience
    /// functions for making the Fountain file importing easier.
    /// </summary>
    class FountainFileLineContainer
    {
        /// <summary>
        /// Fountain file's title page can specify some properties. This class
        /// is used for storing those properties.
        /// </summary>
        class PropertyItem
        {
            private string mKey = "";
            private string mValue = "";

            public string Key
            {
                get { return mKey; }
                set { mKey = value; }
            }

            public string Value
            {
                get { return mValue; }
                set { mValue = value; }
            }

            public PropertyItem()
            {

            }

            public PropertyItem(string key, string val)
            {
                mKey = key;
                mValue = val;
            }

            public void AppendValue(string dataToAppend)
            {
                mValue = mValue + dataToAppend;
            }
        }


        private List<string> mTextLines = new List<string>();
        private List<PropertyItem> mProperties = new List<PropertyItem>();
        private int mReadPos = 0;
        private bool mTitlePagePhase = true;
        private int mEmptyLinesInARow = 0;
        private string mPreviousProperty = "";


        /// <summary>
        /// Constructor.
        /// </summary>
        public FountainFileLineContainer()
        {

        }


        /// <summary>
        /// Adds the given list of text files to this container.
        /// </summary>
        /// <param name="textLines">The lines to be added to this container.</param>
        public void AddTextLines(List<string> textLines)
        {
            foreach (var l in textLines)
            {
                AddLine(l.Trim());
            }
        }


        /// <summary>
        /// Adds a single text line to this container.
        /// </summary>
        /// <param name="l">Contents of the line to be added.</param>
        public void AddLine(string l)
        {
            if (l != null)
            {
                if (l.Length == 0)
                {
                    mEmptyLinesInARow++;
                    if (mEmptyLinesInARow == 2)
                    {
                        // Now there has been 2 empty lines in a row. This will end the
                        // title page phase and following added lines no longer can
                        // contain property values.
                        mTitlePagePhase = false;
                    }
                }
                else
                {
                    mEmptyLinesInARow = 0;
                    if (mTitlePagePhase)
                    {
                        int colonIndex = l.IndexOf(":");
                        if (colonIndex >= 0)
                        {
                            string propName = l.Substring(0, colonIndex);
                            string propVal = l.Substring(colonIndex + 1);
                            var p = new PropertyItem(propName.Trim(), propVal.Trim());
                            mProperties.Add(p);
                            mPreviousProperty = propName.Trim();
                            // No need to add property line to line array.
                            return;
                        }
                        else
                        {
                            if (l.StartsWith("   ") || l.StartsWith("\t"))
                            {
                                var p = this.FindPropertyItem(mPreviousProperty);
                                if (p != null)
                                {
                                    p.AppendValue(l.Trim());
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        // Title page phase is over. Let the execution flow through and add
                        // the line to lines array.
                    }
                    
                }
                
                mTextLines.Add(l);
            }
        }


        /// <summary>
        /// Clears all the contents.
        /// </summary>
        public void Clear()
        {
            mTextLines.Clear();
        }


        /// <summary>
        /// Returns the number of lines this container has.
        /// </summary>
        public int LineCount
        {
            get { return mTextLines.Count; }
        }


        /// <summary>
        /// Returns the number of lines there is from current read position
        /// to the last line.
        /// </summary>
        /// <returns>Number of lines from current read position to the end.</returns>
        public int LinesLeft()
        {
            return mTextLines.Count - mReadPos;
        }


        /// <summary>
        /// Sets the read position to given line number.
        /// </summary>
        /// <param name="lineNum">The line number that becomes our new read position.</param>
        public void SetReadPosition(int lineNum)
        {
            mReadPos = lineNum;
        }


        /// <summary>
        /// Returns the next line from this container and moves the read position by one.
        /// </summary>
        /// <returns>The line from current read position.</returns>
        public string ReadLineAndAdvance()
        {
            return mTextLines[mReadPos++];
        }


        /// <summary>
        /// Returns the text contents of given line number.
        /// </summary>
        /// <param name="lineIndex">The line number to be returned.</param>
        /// <returns>The contents of given line number.</returns>
        public string GetLine(int lineIndex)
        {
            return mTextLines[lineIndex];
        }


        /// <summary>
        /// Returns the line contents from current read position plus lineOffset-parameter.
        /// </summary>
        /// <param name="lineOffset">The line offset that will be added to current
        /// read position line number.</param>
        /// <returns>The requested line contents.</returns>
        public string PeekLine(int lineOffset)
        {
            if (mReadPos + lineOffset < 0 || mReadPos + lineOffset >= mTextLines.Count)
            {
                // Indexing non-existing line.
                return "";

            }
            return mTextLines[mReadPos + lineOffset];
        }


        /// <summary>
        /// Moves the read position by one.
        /// </summary>
        /// <returns>Returns true if container's end was reached.</returns>
        public bool StepForward()
        {
            if (mReadPos < 0 || mReadPos >= mTextLines.Count)
            {
                return true;
            }

            ++mReadPos;
            return mReadPos >= mTextLines.Count();
        }


        /// <summary>
        /// Moves the current read position back by one.
        /// </summary>
        /// <returns>Returns true if container's head was reached.</returns>
        public bool StepBackward()
        {
            if (mReadPos < 0 || mReadPos >= mTextLines.Count)
            {
                return true;
            }

            --mReadPos;
            return mReadPos < 0;
        }


        /// <summary>
        /// Sets the read position to the begining of the lines list.
        /// </summary>
        public void SeekFirst()
        {
            if (mTextLines.Count > 0)
            {
                mReadPos = 0;
            }
            else
            {
                mReadPos = -1;
            }
        }


        /// <summary>
        /// Sets the read position to the very last line of this container.
        /// </summary>
        public void SeekLast()
        {
            if (mTextLines.Count > 0)
            {
                mReadPos = mTextLines.Count - 1;
            }
            else
            {
                mReadPos = -1;
            }
        }


        /// <summary>
        /// Tells if there are more lines to be read from this container.
        /// </summary>
        /// <returns>Returns true if unread lines are left.</returns>
        public bool OkToRead()
        {
            if (mReadPos < 0 || mReadPos >= mTextLines.Count)
            {
                return false;
            }
            return true;
        }
                

        /// <summary>
        /// Tells if there is an empty line in current read location plus given
        /// offset value.
        /// </summary>
        /// <param name="lineOffset">The offset to current read location.</param>
        /// <returns>Returns true if line is empty.</returns>
        public bool IsEmptyLine(int lineOffset)
        {
            if (mReadPos + lineOffset < 0 || mReadPos + lineOffset >= mTextLines.Count)
            {
                return false;
            }
            string l = GetLine(lineOffset);
            return l.Length == 0 ? true : false;
        }


        /// <summary>
        /// Returns the given property.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="valueIfNotFound">The value that will be returned if property was not found.</param>
        /// <returns>The value of the property.</returns>
        public string GetProperty(string propertyName, string valueIfNotFound)
        {
            var propertyItem = FindPropertyItem(propertyName);
            if (propertyItem != null)
            {
                return propertyItem.Value;
            }
            return valueIfNotFound;
        }


        private PropertyItem FindPropertyItem(string propName)
        {
            foreach (var p in mProperties)
            {
                if (p.Key.Equals(propName))
                {
                    return p;
                }
            }
            return null;
        }

    }
}
