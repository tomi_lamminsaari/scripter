﻿using Scripter.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.DataModel.Converters
{
    class FountainImporter : ImporterBase
    {

        private ScriptTextRecognizer recognizer = new ScriptTextRecognizer();
        private FountainFileLineContainer mLineContainer = new FountainFileLineContainer();

        public FountainImporter()
        {

        }


        public override async Task ImportAsync(ScriptDocument doc, Windows.Storage.StorageFile inputFile)
        {

            var reader = await GetReader(inputFile);
            var ret = await reader.LoadAsync(1000000); // TODO: More clever byte value.

            // Read lines from the fountain file to line container object.
            var lines = ReadAllTheLines(reader);
            mLineContainer.AddTextLines(lines);
            lines.Clear();

            // Start importing the data from read buffer to actual object hierarchy.
            Debug.WriteLine("FountainImporter: Start importing script document...");
            await Task.Run(() =>
            {
                ImportTitlePage(doc, mLineContainer);
                ImportScenes(doc, mLineContainer);
            });
            Debug.WriteLine("FountainImporter: Script document importing complete!");

        }


        private void ImportTitlePage(ScriptDocument doc, FountainFileLineContainer lines)
        {
            string title = lines.GetProperty("Title", "");
            string author = lines.GetProperty("Author", "");

            doc.ScriptTitle = title;
            doc.Authors = author;
            Debug.WriteLine("Title: " + title);
            Debug.WriteLine("Author: " + author);
        }


        private string FindTitlePageProperty(string property, List<string> lines)
        {
            foreach(string str in lines)
            {
                int index = str.IndexOf(property);
                if (index == 0)
                {
                    int colonPos = str.IndexOf(":");
                    if (colonPos > 0)
                    {
                        string titleStr = str.Substring(colonPos + 1).Trim();
                        return titleStr;
                    }
                }
            }
            return "";
        }


        private void ImportScenes(ScriptDocument doc, FountainFileLineContainer lineContainer)
        {
            /*
            lineContainer.SeekFirst();

            Scene activeScene = null;
            Dialoque activeDialogue = null;
            SceneAction activeAction = null;
            
            while (lineContainer.OkToRead())
            {
                var l = lineContainer.ReadLineAndAdvance();
                if (IsSceneHeadingLine(l))
                {
                    activeScene = doc.NewScene();
                    activeDialogue = null;
                    activeAction = null;

                    // Use the parser to parse the scene header components.
                    bool err = false;
                    var headingDict = this.recognizer.GetParsedSceneHeader(l, out err);
                    string intExt = "";
                    string loc = "";
                    string tod = "";
                    headingDict.TryGetValue(ScriptTextRecognizer.SceneHeaderPartIntExt, out intExt);
                    headingDict.TryGetValue(ScriptTextRecognizer.SceneHeaderPartLocation, out loc);
                    headingDict.TryGetValue(ScriptTextRecognizer.SceneHeaderPartTimeOfDay, out tod);

                    activeScene.SetHeadingText(intExt, loc, tod);

                }
                else if (IsCharacterLine(l) && activeScene != null)
                {
                    // Current line is a person line.
                    activeDialogue = doc.AddDialog(activeScene, l);

                }
                else if (activeDialogue != null && l.Length > 0)
                {
                    // We have active dialog line. It means that this line contain text that is
                    // related to that dialog. Create new dialog and stop appending data to
                    // possible action element.
                    activeDialogue.AddDialogLine(l);
                    activeAction = null;

                }
                else if (l.Length == 0)
                {
                    // An empty line. Dialog ends, action ends.
                    activeDialogue = null;
                    activeAction = null;
                }
                else
                {
                    // In other cases this is an action-element. Stop editing
                    // dialog elements.
                    activeDialogue = null;
                    if (activeScene != null)
                    {
                        if (activeAction == null)
                        {
                            activeAction = activeScene.AddAction(l);
                        }
                        else
                        {
                            // We have active action. Append this line there.
                            activeAction.AppendText(l);
                        }
                    }
                }
            }
            */
        }

       
        private static bool IsSceneHeadingLine(string line)
        {
            bool ret = false;
            /*
            int[] sceneHeadingIdentifierArray = new int[5];
            sceneHeadingIdentifierArray[0] = line.IndexOf("EXT.");
            sceneHeadingIdentifierArray[1] = line.IndexOf("INT.");
            sceneHeadingIdentifierArray[2] = line.IndexOf("EXT/INT.");
            sceneHeadingIdentifierArray[3] = line.IndexOf("INT/EXT.");
            sceneHeadingIdentifierArray[4] = line.IndexOf(".");
            int doubleDots = line.IndexOf("..");
            if (doubleDots == 0)
            {
                // Line begins with multiple period-symbols. It can't be scene heading line.
                sceneHeadingIdentifierArray[4] = -1;
            }
            foreach (int i in sceneHeadingIdentifierArray)
            {
                if (i == 0)
                {
                    ret = true;
                    break;
                }
            }
            */
            return ret;
        }


        private static bool IsCharacterLine(string line)
        {
            bool uc = StringParseUtils.OnlyUpperCaseCharacters(line);
            
            int endsWithTO = line.IndexOf("TO:");
            if (endsWithTO >= line.Length - 4)
            {
                // At the end of line there is "TO:". This means that this line
                // contains a transition element.
                return false;
            }
            
            return uc;
            
        }


        private static bool IsTransitionLine(string line)
        {
            bool uc = StringParseUtils.OnlyUpperCaseCharacters(line);
            int endsWithTO = line.IndexOf("TO:");
            if (endsWithTO >= line.Length - 4 && uc)
            {
                return true;
            }
            return false;
        }


        private static bool IsParenthetical(string line)
        {
            if (line == null || line.Length == 0)
            {
                return false;
            }
            bool ret = false;
            try
            {
                string trimmed = line.Trim();
                int i1 = trimmed.IndexOf("(");
                int i2 = trimmed.IndexOf(")", 1);
                if (i1 >= 0 && i2 >= 0 && i1 < i2)
                {
                    ret = true;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("ERROR !!! IsParenthetical");
            }
            return ret;
        }
    }
}
