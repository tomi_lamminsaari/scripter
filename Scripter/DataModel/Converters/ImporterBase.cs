﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.DataModel.Converters
{
    class ImporterBase
    {

        public virtual async Task ImportAsync(ScriptDocument doc, Windows.Storage.StorageFile inputFile)
        {
            await Task.Run(() =>
            {
                // Todo: Some work here!
            });
        }


        protected ImporterBase()
        {

        }


        protected async Task<Windows.Storage.Streams.DataReader> GetReader(Windows.Storage.StorageFile inputFile)
        {
            var readStream = await inputFile.OpenAsync(Windows.Storage.FileAccessMode.Read);
            var reader = new Windows.Storage.Streams.DataReader(readStream);
            return reader;
        }


        protected List<String> ReadAllTheLines(Windows.Storage.Streams.DataReader reader)
        {
            var sb = new StringBuilder();
            var lines = new List<string>();
            reader.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;
            try {
                byte[] workBuffer = null;
                if (reader.UnconsumedBufferLength > 0)
                {
                    workBuffer = new byte[reader.UnconsumedBufferLength];
                }

                int workIndex = 0;
                while (reader.UnconsumedBufferLength > 0)
                {
                    byte b = reader.ReadByte();
                    if (b == '\n')
                    {
                        // Line change found.
                        string tmpTxt = System.Text.Encoding.UTF8.GetString(workBuffer, 0, workIndex);
                        lines.Add(tmpTxt);
                        workIndex = 0;
                    }
                    else
                    {
                        workBuffer[workIndex++] = b;
                    }


                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Bad happened while reading!!!");
            }
            return lines;
        }
    }
}
