﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.System.Threading;

namespace Scripter.DataModel.Converters
{
    class ScriptExporter
    {
        public enum ExportError
        {
            NoError,
            FileError,
            UnspecifiedError
        }


        private ExportError mError = ExportError.NoError;
        private ExporterBase mExporterImpl;
        private ScriptDocument mDoc;
        private Windows.Storage.Streams.DataWriter mWriter;


        public static ScriptExporter CreateExporter(string format)
        {
            ScriptExporter exporter = new ScriptExporter();

            if (format == "fountain")
            {
                exporter.SetExpoterImpl(new FountainExporter());
            }
            else if (format == "xml")
            {
                exporter.SetExpoterImpl(new XmlExporter());
            }

            return exporter;
        }


        public async Task<ExportError> Export(ScriptDocument doc, Windows.Storage.Streams.DataWriter writer)
        {
            mDoc = doc;
            mWriter = writer;

            ExportError err = ExportError.NoError;
            await mExporterImpl.ExportAsync(doc, writer);

            return err;
        }

        public void Cancel()
        {

        }
        

        public ScriptExporter()
        {

        }

        private void SetExpoterImpl(ExporterBase exp)
        {
            mExporterImpl = exp;
        }
    }
}
