﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.DataModel.Converters
{
    class ScriptImporter
    {

        private ImporterBase mImporter;

        public static ScriptImporter CreateImporter(string inputFormat)
        {
            var importer = new ScriptImporter();

            if (inputFormat == "fountain")
            {
                var impl = new FountainImporter();
                importer.SetImporterImpl(impl);
            }

            return importer;
        }


        public async Task<int> ImportAsync(ScriptDocument doc, Windows.Storage.StorageFile inputFile)
        {
            int ret = 0;

            if (mImporter != null)
            {
                await mImporter.ImportAsync(doc, inputFile);
            }
            else
            {
                ret = -1;
            }

            return ret;
        }


        public void SetImporterImpl(ImporterBase importer)
        {
            mImporter = importer;
        }


        ScriptImporter()
        {

        }
    }
}
