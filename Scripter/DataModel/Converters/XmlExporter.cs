﻿using Scripter.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Scripter.DataModel.Converters
{
    class XmlExporter : ExporterBase
    {
        public XmlExporter()
        {

        }

        public override async Task ExportAsync(ScriptDocument doc, Windows.Storage.Streams.DataWriter writer)
        {
            // Prepare XML document from given movie script object. Create XML declaration
            // and the root level moviescript-tags.            
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(xmlDeclaration);
            XmlElement rootElement = xmlDoc.CreateElement("moviescript");
            xmlDoc.AppendChild(rootElement);
            
            // Create the script info heading part of the XML document.
            CreateScriptInfo(doc, xmlDoc, rootElement);

            // Create the people list.
            CreatePeopleSection(doc, xmlDoc, rootElement);

            // Create the Scenes
            CreateScenesSection(doc, xmlDoc, rootElement);

            // Write the generated XML document to the given file.
            var buffer = Windows.Security.Cryptography.CryptographicBuffer.ConvertStringToBinary(
                xmlDoc.OuterXml, Windows.Security.Cryptography.BinaryStringEncoding.Utf8);
            writer.WriteBuffer(buffer);
            await writer.StoreAsync();
        }


        private void CreateScriptInfo(ScriptDocument doc, XmlDocument xmlDoc, XmlNode movieScriptElement)
        {
            XmlElement scriptInfoElement = xmlDoc.CreateElement("scriptinfo");
            movieScriptElement.AppendChild(scriptInfoElement);

            // Set date to the XML document.
            XmlElement scriptDateElement = xmlDoc.CreateElement("scriptdate");
            scriptDateElement.AppendChild(xmlDoc.CreateTextNode(doc.ScriptDate.ToString()));
            scriptInfoElement.AppendChild(scriptDateElement);

            // Set the script authors list.
            XmlElement authorsElement = xmlDoc.CreateElement("authors");
            authorsElement.AppendChild(xmlDoc.CreateTextNode(doc.Authors));
            scriptInfoElement.AppendChild(authorsElement);

            // Set the script copyright text.
            XmlElement copyrightElement = xmlDoc.CreateElement("copyright");
            copyrightElement.AppendChild(xmlDoc.CreateTextNode(doc.Copyright));
            scriptInfoElement.AppendChild(copyrightElement);
        }


        private void CreatePeopleSection(ScriptDocument doc, XmlDocument xmlDoc, XmlNode movieScriptElement)
        {
            XmlElement peopleElement = xmlDoc.CreateElement("people");
            movieScriptElement.AppendChild(peopleElement);

            // Add each person to the people-section of the xml.
            var personList = doc.GetCharacterNameList();
            /*
            foreach (var personName in personList)
            {
                SceneElementPerson p = doc.FindPersonByName(personName);

                XmlElement personElement = xmlDoc.CreateElement("person");
                personElement.SetAttribute("id", "aaa");
                personElement.SetAttribute("name", p.Name);
                personElement.SetAttribute("picture", p.PictureName);
                personElement.SetAttribute("story", p.Story);
                peopleElement.AppendChild(personElement);
            }
            */
        }


        private void CreateScenesSection(ScriptDocument doc, XmlDocument xmlDoc, XmlNode movieSriptElement)
        {
            XmlElement scenesElement = xmlDoc.CreateElement("scenes");
            movieSriptElement.AppendChild(scenesElement);

            var scenesList = doc.ScenesList;
            foreach (var s in scenesList)
            {
                XmlElement sceneElement = xmlDoc.CreateElement("scene");
                SceneHeading sceHead = s.SceneHeadingInfo;

                // Set the scene heading.
                sceneElement.SetAttribute("n", s.SceneNumber);
                if (sceHead.PrimaryLocation != null && StringParseUtils.IsEmptyString(sceHead.PrimaryLocation) == false)
                {
                    sceneElement.SetAttribute("h1", sceHead.PrimaryLocation);
                }
                if (sceHead.Location != null && StringParseUtils.IsEmptyString(sceHead.Location) == false)
                {
                    sceneElement.SetAttribute("h2", sceHead.Location);
                }
                if (sceHead.HasTimeOfDay())
                {
                    sceneElement.SetAttribute("h3", sceHead.TimeOfDay);
                }
                
                // Append the scene elements to the scene.
                foreach (var se in s.SceneElementObjects)
                {
                    CreateSceneChildElement(se, xmlDoc, sceneElement);
                }

                // Set generic scene properties
                if (s.TheEndFlag)
                {
                    sceneElement.SetAttribute("end", "true");
                }
                

                scenesElement.AppendChild(sceneElement);
            }
        }

        private void CreateSceneChildElement(SceneElement elem, XmlDocument xmlDoc, XmlElement sceneElement)
        {
            /*
            if (elem as SceneAction != null)
            {
                
                var sceneAction = elem as SceneElementAction;
                XmlElement xmlActionElement = xmlDoc.CreateElement("action");
                xmlActionElement.AppendChild(xmlDoc.CreateTextNode(sceneAction.GetAsString()));
                sceneElement.AppendChild(xmlActionElement);
                
            }
            else if (elem as SceneElementDialog != null)
            {
/*                var sceneDialog = elem as SceneElementDialog;
                XmlElement xmlDialogElement = xmlDoc.CreateElement("dialog");
                xmlDialogElement.SetAttribute("person", sceneDialog.PersonName);

                for (int i=0; i < sceneDialog.GetLineCount(); ++i)
                {
                    string dialogLine = sceneDialog.GetLineByIndex(i);
                    XmlElement xmlDialogLine = xmlDoc.CreateElement("dline");
                    xmlDialogLine.AppendChild(xmlDoc.CreateTextNode(dialogLine));
                    xmlDialogElement.AppendChild(xmlDialogLine);
                }

                sceneElement.AppendChild(xmlDialogElement);
  
            }
            */
        }
        
    }
}
