﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Scripter.DataModel
{
    /// <summary>
    /// A class that works as a manager that gives access to all scripts that
    /// have been created with this application.
    /// </summary>
    class DocumentManager
    {

        public class ScriptDocumentItem
        {
            public string DocumentId { get; set; }
            public string DocumentPath { get; set; }

            public ScriptDocumentItem(string documentId, string filePath)
            {
                this.DocumentId = documentId;
                this.DocumentPath = filePath;
            }
        }

        public const string SCRIPTS_FOLDER = "MovieScripts/";

        private ScriptDocument mActiveDocument = null;

        Dictionary<string, ScriptDocument> mDocumentsArray = new Dictionary<string, ScriptDocument>();
        private List<ScriptDocumentItem> mRecentDocuments = new List<ScriptDocumentItem>();
        private List<ScriptDocumentItem> mAllDocuments = new List<ScriptDocumentItem>();
        

        public DocumentManager()
        {
            
        }

        public async void RefreshDocuments()
        {
            bool refreshSuccess = await RefreshFilesList();
            if (refreshSuccess == false)
            {
                Debug.WriteLine("Refreshing documents list failed!");
            }
        }

        /// <summary>
        /// Creates a unique identifier for the script document.
        /// </summary>
        /// <returns>Returns a string that contains unique document identifier.</returns>
        public static string CreateDocumentId()
        {
            char[] chars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
            char[] guidChars = new char[8];
            Random rnd = new Random();
            for (int i = 0; i < guidChars.Length; ++i)
            {
                int charIndex = rnd.Next(chars.Length);
                guidChars[i] = chars[charIndex];
            }
            return new string(guidChars);
        }

        public ScriptDocument CreateNewDocument(string documentId)
        {
            var scriptDoc = FindScriptDocument(documentId);
            if (scriptDoc != null)
            {
                Debug.WriteLine("DocumentManager.CreateNewDocument(): ID probably in use already!");
                return null;
            }
            scriptDoc = new ScriptDocument();
            scriptDoc.SetDocumentId(documentId);

            // TODO: Remove these!
            //Scene s1 = scriptDoc.NewScene();
            //s1.SetHeadingText("INT.", "JOHN'S APPARTMENT", "");
            //s1.AddAction("JOHN took his cake and ate it.");
            //s1.AddDialog("JOHN", "", "Want more cake?");
            //s1.AddDialog("SUSAN", "", "No thanks.");

            //mDocumentsArray.Add(documentId, scriptDoc);
            this.mActiveDocument = scriptDoc;
            return scriptDoc;
        }

        /// <summary>
        /// Returns the currently active movie script.
        /// </summary>
        /// <returns>Currently active movie script.</returns>
        public ScriptDocument GetActiveDocument()
        {
            return this.mActiveDocument;
        }


        public void AddRecentDocument(string documentId)
        {

        }


        public ScriptDocument FindScriptDocument(string documentId)
        {
            ScriptDocument retDoc = null;
            if (mDocumentsArray.ContainsKey(documentId))
            {
                bool ret = mDocumentsArray.TryGetValue(documentId, out retDoc);
                return retDoc;
            }
            return retDoc;
        }


        private async Task<bool> RefreshFilesList()
        {
            // Ensure the storage folder for movie scripts exists.
            bool success = false;
            StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            StorageFolder workDir = await localFolder.CreateFolderAsync(SCRIPTS_FOLDER, CreationCollisionOption.OpenIfExists);

            // Scan the movie scripts from opened working directory.
            IReadOnlyList<StorageFile> filesList = await workDir.GetFilesAsync();
            mAllDocuments.Clear();
            foreach (var fileEntry in filesList)
            {
                var documentEntry = new ScriptDocumentItem(fileEntry.Name, "");
                mAllDocuments.Add(documentEntry);
            }
            success = true;

            return success;
        }

    }
}
