﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.DataModel.Index
{
    class LocationIndex
    {

        class LocationIndexItem
        {
            public List<Scene> mSceneList = new List<Scene>();
        }

        Dictionary<string, LocationIndexItem> mSceneHeadingIndex = new Dictionary<string, LocationIndexItem>();
        

        public LocationIndex()
        {

        }

        public void ClearIndex()
        {
            mSceneHeadingIndex.Clear();
        }

        public void AddLocation(string location, Scene scene)
        {
            LocationIndexItem indexItem;
            bool found = mSceneHeadingIndex.TryGetValue(location, out indexItem);
            if (found)
            {
                indexItem.mSceneList.Add(scene);
            }
            else
            {
                indexItem = new LocationIndexItem();
                indexItem.mSceneList.Add(scene);
                mSceneHeadingIndex.Add(location, indexItem);
            }
        }

        public void RemoveLocation(string location, Scene scene)
        {
            LocationIndexItem indexItem;
            bool found = mSceneHeadingIndex.TryGetValue(location, out indexItem);
            if (found)
            {
                indexItem.mSceneList.Remove(scene);
                if (indexItem.mSceneList.Count == 0)
                {
                    // No more scenes left in scene list. Remove the whole location index.
                    mSceneHeadingIndex.Remove(location);
                }
            }
        }


        public List<Scene> GetScenesInLocation(string location, bool partialSearch)
        {
            LocationIndexItem indexItem;
            bool found = mSceneHeadingIndex.TryGetValue(location, out indexItem);
            if (found)
            {
                return indexItem.mSceneList;
            }
            else
            {
                return new List<Scene>();
            }
        }


        public List<string> GetLocations()
        {
            List<string> tmpLocations = new List<string>();
            foreach (var indexItem in mSceneHeadingIndex.Keys)
            {
                tmpLocations.Add(indexItem);
            }
            return tmpLocations;
        }

    }
}
