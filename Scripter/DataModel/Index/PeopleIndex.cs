﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Scripter.DataModel.Index
{

    /// <summary>
    /// PeopleIndex is a container that keeps track on character names
    /// that have been used in movie script.
    /// </summary>
    class PeopleIndex
    {

        /// <summary>
        /// This class is used for associating the person object with
        /// the Scene objects.
        /// </summary>
        public class PersonSceneAssociation
        {
            private List<Scene> mAppearInScenes = new List<Scene>();

            public void AssociateWithScene(Scene scene)
            {
                if (!mAppearInScenes.Contains(scene))
                {
                    mAppearInScenes.Add(scene);
                }
            }

            public int DisassociateWithScene(Scene scene)
            {
                mAppearInScenes.Remove(scene);
                return mAppearInScenes.Count;
            }

            public int NumberOfAssociatedScenes
            {
                get { return mAppearInScenes.Count; }
            }

            public List<Scene> GetSceneList()
            {
                return mAppearInScenes;
            }
        }


        private Dictionary<string, SceneElementPerson> mPersons = new Dictionary<string, SceneElementPerson>();
        private Dictionary<SceneElementPerson, PersonSceneAssociation> mPersonSceneAssociations = new Dictionary<SceneElementPerson, PersonSceneAssociation>();
        private ObservableCollection<string> mPersonNames = new ObservableCollection<string>();
        private bool mModificationsOngoing = false;

        /// <summary>
        /// Constructor.
        /// </summary>
        public PeopleIndex()
        {

        }


        /// <summary>
        /// Removes all the persons from this index.
        /// </summary>
        public void ClearIndex()
        {
            mPersonSceneAssociations.Clear();
        }


        /// <summary>
        /// Adds new person to the index.
        /// </summary>
        /// <param name="characterName">Name of the person to be added.</param>
        /// <returns>Reference to new person object.</returns>
        public SceneElementPerson AddCharacter(string characterName)
        {
            SceneElementPerson retPerson = null;

            var person = FindPerson(characterName);
            if (person == null)
            {
                retPerson = new SceneElementPerson(characterName);
                mPersons.Add(characterName, retPerson);

                RefreshPersonNameList();
            }
            else
            {
                retPerson = person;
            }
            return retPerson;
        }


        public void AddCharactersFromScene(Scene scene)
        {
            List<SceneElement> sceneElements = scene.GetSceneElements();
            if (sceneElements != null)
            {
                foreach (var elem in sceneElements)
                {
                    SceneElementPerson personElem = elem as SceneElementPerson;
                    if (personElem != null)
                    {
                        var person = FindPerson(personElem.GetAsString());
                        if (person == null)
                        {
                            mPersons.Add(personElem.GetAsString(), person);
                        }
                    }
                }
            }
            
        }


        public ObservableCollection<string> PersonNameList
        {
            get { return mPersonNames; }
        }



        public SceneElementPerson FindPerson(string name)
        {
            SceneElementPerson retValue = null;
            bool err = mPersons.TryGetValue(name, out retValue);
            return retValue;
        }

        public List<string> GetMatchingCharacterNames(string partialName)
        {
            List<string> chaList = new List<string>();
            // TODO: This function must be implemented!
            Debug.WriteLine("PeopleINdex.GetMatchingCharacterNames(): Function not implemented!!!");
            return chaList;
        }

        public void AssociatePersonAndScene(SceneElementPerson person, Scene scene)
        {
            PersonSceneAssociation associateObj = null;
            bool success = mPersonSceneAssociations.TryGetValue(person, out associateObj);
            if (success)
            {
                associateObj.AssociateWithScene(scene);
            }
            else
            {
                associateObj = new PersonSceneAssociation();
                associateObj.AssociateWithScene(scene);
                mPersonSceneAssociations.Add(person, associateObj);
            }
        }

        public void DisassociatePersonAndScene(SceneElementPerson person, Scene scene)
        {
            PersonSceneAssociation associateObj = null;
            bool success = mPersonSceneAssociations.TryGetValue(person, out associateObj);
            if (success)
            {
                int sceneCount = associateObj.DisassociateWithScene(scene);
                if (sceneCount == 0)
                {
                    mPersonSceneAssociations.Remove(person);
                }
            }
        }

        public List<Scene> GetPersonsSceneList(SceneElementPerson person)
        {
            PersonSceneAssociation assObj = null;
            bool success = mPersonSceneAssociations.TryGetValue(person, out assObj);
            if (success)
            {
                return assObj.GetSceneList();
            }
            else
            {
                return new List<Scene>();
            }
        }


        public void RenamePerson(SceneElementPerson person, string newName)
        {
            /*
            PersonSceneAssociation assObject = null;
            bool success = mPersonSceneAssociations.TryGetValue(person, out assObject);
            if (!success || assObject == null)
            {
                Debug.WriteLine("PeopleIndex.RenamePerson(): Error! Person not found!");
            }

            mPersonSceneAssociations.Remove(person);
            mPersons.Remove(person.Name);

            person.Name = newName;
            mPersons.Add(newName, person);
            mPersonSceneAssociations.Add(person, assObject);

            RefreshPersonNameList();
            */
        }


        private void RefreshPersonNameList()
        {
            mPersonNames.Clear();
            foreach (var k in mPersons.Keys)
            {
                mPersonNames.Add(k);
            }
        }
    }
}
