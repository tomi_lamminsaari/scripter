﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Scripter.DataModel
{
    class MovieScriptManager
    {
        public interface IScriptScanObserver
        {
            void ScriptScanComplete();
        }

        class ScriptFileItem
        {
            public string Path { get; set; }
            public string Id { get; set; }

            public ScriptFileItem(string path, string id)
            {
                this.Path = path;
                this.Id = id;
            }
        }


        private List<ScriptFileItem> mScriptFiles = new List<ScriptFileItem>();
        private List<IScriptScanObserver> mScriptScanObservers = new List<IScriptScanObserver>();
        private Object mScriptScanLock = new Object();


        /// <summary>
        /// Creates a unique identifier for the script document.
        /// </summary>
        /// <returns>Returns a string that contains unique document identifier.</returns>
        public static string CreateDocumentId()
        {
            char[] chars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
            char[] guidChars = new char[8];
            Random rnd = new Random();
            for (int i = 0; i < guidChars.Length; ++i)
            {
                int charIndex = rnd.Next(chars.Length);
                guidChars[i] = chars[charIndex];
            }
            return new string(guidChars);
        }

        public MovieScriptManager()
        {

        }

        public ScriptDocument FindMovieScript(string scriptId)
        {
            ScriptDocument script = null;
            return script;
        }


        public void ScanScripts()
        {
            Task.Run(async () =>
            {
                await ScanMovieScriptsDir();
            });
        }


        public void RegisterScriptScanObserver(IScriptScanObserver observer)
        {
            lock(mScriptScanLock)
            {
                int index = mScriptScanObservers.IndexOf(observer);
                if (index < 0)
                {
                    mScriptScanObservers.Add(observer);
                }

            }
        }


        public void UnregisterScriptScanObserver(IScriptScanObserver observer)
        {
            lock(mScriptScanLock)
            {
                mScriptScanObservers.Remove(observer);
            }
        }

        private async Task ScanMovieScriptsDir()
        {
            StorageFolder saveFolder = Windows.Storage.ApplicationData.Current.LocalFolder;

            string workDirName = "MovieScripts";
            var rootPath = saveFolder.Path;
            var scriptsPath = rootPath + "\\" + workDirName;
            StorageFolder workingFolder = null;
            try
            {
                workingFolder = await StorageFolder.GetFolderFromPathAsync(scriptsPath);
            }
            catch (System.IO.FileNotFoundException e)
            {
                // The folder does not exist. Create it now.
                workingFolder = await saveFolder.CreateFolderAsync(workDirName);
            }

            // Use the resolved storage folder to get all the files from that folder.
            string tmpId = CreateDocumentId();
            IReadOnlyList < StorageFile > files = await workingFolder.GetFilesAsync();
            foreach (var f in files)
            {
                string path = f.Path;
                string id = f.Name.Substring(0, tmpId.Length);

                this.mScriptFiles.Add(new DataModel.MovieScriptManager.ScriptFileItem(path, id));
            }

            // Notify the observers that the scan is complete. Create temporary array of the observers
            // to prevent deadlock if callee wants to unregister the observer in callback.
            IScriptScanObserver[] observers = new IScriptScanObserver[this.mScriptScanObservers.Count];
            lock (mScriptScanLock)
            {
                this.mScriptScanObservers.CopyTo(observers);
            }
            foreach (var obs in observers)
            {
                obs.ScriptScanComplete();
            }
        }
    }
}
