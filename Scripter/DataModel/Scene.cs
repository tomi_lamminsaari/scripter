﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Scripter.DataModel
{
    /// <summary>
    /// Scene class is the container for all the data that relates
    /// to a one single scene.
    /// </summary>
    public class Scene : INotifyPropertyChanged
    {
        private int mNumber;
        private SceneHeading mHeading = new SceneHeading();
        private SceneElementAction mAction = new SceneElementAction();
        private List<SceneElement> mSceneElements = new List<SceneElement>();
        private ScriptDocument mParentDocument;
        private bool mTheEndScene = false;
        private int mEstimatedLength = 30;

        
        //
        // Bindable properties
        //

        /// <summary>
        /// A property that returns the INT., EXT. location of the
        /// scene.
        /// </summary>
        public string EnvIndicator
        {
            get { return mHeading.PrimaryLocation; }
        }


        /// <summary>
        /// A property that returns the scene heading text with name
        /// and time of day.
        /// </summary>
        public String Heading
        {
            get
            {
                if (mHeading.HasTimeOfDay())
                {
                    return mHeading.Location + " - " + mHeading.TimeOfDay;
                }
                else
                {
                    return mHeading.Location;
                }
            }
        }

        /// <summary>
        /// A property that returns combined heading text. For example:
        /// INT. CENTRAL PARK - MORNING
        /// </summary>
        public string CombinedHeaderText
        {
            get
            {
                var sb = new StringBuilder();
                sb.Append(mHeading.PrimaryLocation);
                sb.Append(" ");
                sb.Append(mHeading.Location);
                if (mHeading.HasTimeOfDay())
                {
                    sb.Append(" - ");
                    sb.Append(mHeading.TimeOfDay);
                }
                return sb.ToString();
            }
        }


        /// <summary>
        /// A property that returns scene number in textual format.
        /// </summary>
        public string SceneNumber
        {
            get { return mNumber.ToString(); }
            set { mNumber = Int32.Parse(value); }
        }


        /// <summary>
        /// A flag that tells if this is the last scene in the
        /// script.
        /// </summary>
        public bool TheEndFlag
        {
            get { return mTheEndScene; }
            set { mTheEndScene = value; }
        }


        /// <summary>
        /// Property for accessing the scene heading object.
        /// </summary>
        public SceneHeading SceneHeadingInfo
        {
            get { return mHeading; }
            set { mHeading = value; }
        }


        /// <summary>
        /// Property for accessing the estimated length of this scene.
        /// </summary>
        public int EstimatedLength
        {
            get { return mEstimatedLength; }
            set { mEstimatedLength = value; }
        }


        /// <summary>
        /// Contains the list of scene elements that have been added to this
        /// scene.
        /// </summary>
        public List<SceneElement> SceneElementObjects
        {
            get
            {
                return mSceneElements;
            }
            set
            {
                mSceneElements.Clear();

                foreach (var e in value)
                {
                    mSceneElements.Add(e);
                }
            }
        }


        /// <summary>
        /// This functions creates the "The End" scene that will be the last
        /// scene in every script.
        /// </summary>
        /// <param name="parentDoc">The script document.</param>
        /// <returns></returns>
        public static Scene CreateTheEndScene(ScriptDocument parentDoc)
        {
            Scene s = new Scene(parentDoc);
            s.mTheEndScene = true;
            s.SetSceneNumber(1);
            s.SetHeadingText("", "THE END", null);
            return s;
        }


        /// <summary>
        /// Default constructor.
        /// </summary>
        public Scene()
        {

        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parentDoc">The script document where this scene
        /// belongs to.</param>
        public Scene(ScriptDocument parentDoc)
        {
            mParentDocument = parentDoc;
        }


        /// <summary>
        /// Returns the ScriptDocument that owns this scene object.
        /// </summary>
        /// <returns>Returns reference to the script's root object.</returns>
        public ScriptDocument GetDocument()
        {
            return mParentDocument;
        }

        /// <summary>
        /// Sets the number of this scene.
        /// </summary>
        /// <param name="num">Number of this scene.</param>
        public void SetSceneNumber(int num)
        {
            mNumber = num;
        }

        /// <summary>
        /// Returns the number of this scene.
        /// </summary>
        /// <returns>Returns the number of this scene.</returns>
        public int GetSceneNumber()
        {
            return mNumber;
        }

        /// <summary>
        /// Increases or decreases the scene number by given
        /// amount.
        /// </summary>
        /// <param name="amount">Number that defines how much scene ordering number
        /// will change. Negative value make number to be smaller. Positive value
        /// will make it bigger.</param>
        public void IncOrDecNumber(int amount)
        {
            mNumber += amount;

            NotifyPropertyChanged("SceneNumber");
        }


        /// <summary>
        /// Sets the heading text of this scene.
        /// </summary>
        /// <param name="intExt">Text defining if this is interior or exterior
        /// scene.</param>
        /// <param name="location">The location where this scene happens. This
        /// is basically the name of this scene.</param>
        /// <param name="tod">The time of day text.</param>
        public void SetHeadingText(string intExt, string location, string tod)
        {
            mHeading.PrimaryLocation = intExt;
            mHeading.Location = location;
            mHeading.TimeOfDay = tod;
            NotifyPropertyChanged("Heading");
            NotifyPropertyChanged("EnvIndicator");

            mParentDocument.NotifySceneHeadingUpdated(this);
        }


        /// <summary>
        /// Returns the heading element.
        /// </summary>
        /// <returns>The heading object.</returns>
        public SceneHeading GetSceneHeading()
        {
            return mHeading;
        }


        /// <summary>
        /// Adds new scene element that will become the last scene element
        /// in this scene.
        /// </summary>
        /// <param name="elem">Reference to the scene object.</param>
        public void AddSceneElement(SceneElement elem)
        {
            mSceneElements.Add(elem);
        }


        /// <summary>
        /// Adds new action scene element to the scene.
        /// </summary>
        /// <param name="actionText">The body text of the ACTION scene element.</param>
        public SceneElementAction AddAction(string actionText)
        {
            var act = new SceneElementAction();
            act.Text = actionText;
            AddSceneElement(act);
            return act;
        }


        /// <summary>
        /// Adds new dialog scene element to this scene.
        /// </summary>
        /// <param name="person">Name of the person who speaks.</param>
        public SceneElementDialog AddDialog(string text)
        {
            var d = new SceneElementDialog(text);
            AddSceneElement(d);
            return d;
        }


        /// <summary>
        /// Adds new SHOT element at the end of this scene.
        /// </summary>
        /// <param name="shotType">Type of shot.</param>
        /// <param name="shotDescription">The description text of the shot</param>
        public void AddShot(string shotType, string shotDescription)
        {
            var d = new SceneElementShot(shotType, shotDescription);
            AddSceneElement(d);
        }


        /// <summary>
        /// Adds new TRANSITION element at the end of this scene.
        /// </summary>
        /// <param name="transitionText">The text of transition element</param>
        public void AddTransition(string transitionText)
        {
            var d = new Transition();
            AddSceneElement(d);
        }


        /// <summary>
        /// Returns all the scene elements this scene object has.
        /// </summary>
        /// <returns>Returns a list of scene elements that belong to this
        /// scene.</returns>
        public List<SceneElement> GetSceneElements()
        {
            return mSceneElements;
        }


        /// <summary>
        /// Removes all the scene elements from this scene.
        /// </summary>
        public void ClearSceneElements()
        {
            mSceneElements.Clear();
        }


        /// <summary>
        /// Returns the full scene heading lead by scene number.
        /// </summary>
        /// <returns>Scene number and full heading.</returns>
        public string GetSceneNumberAndHeading()
        {
            string fullHeading = String.Format("{0}. {1}", this.mNumber, this.CombinedHeaderText);
            return fullHeading;
        }


        /// <summary>
        /// Returns the list of person names who have dialog in this scene.
        /// </summary>
        /// <returns>List of person names.</returns>
        public List<string> GetListOfDialogPersons()
        {
            /*
            var personList = new List<string>();

            foreach (var sceneElem in mSceneElements)
            {
                if (sceneElem == null)
                {
                    // TODO: Needs finxing.
                    Debug.WriteLine("NullPointException case when adding scene elements!");
                }
                else
                {
                    if (sceneElem.GetElementType() == SceneElement.ElemType.SETypeDialog)
                    {
                        var dialogue = (SceneElementDialog)sceneElem;
                        if (!personList.Contains(dialogue.PersonName))
                        {
                            personList.Add(dialogue.PersonName);
                        }

                    }
                }
                
            }
            return personList;
            */
            return null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
