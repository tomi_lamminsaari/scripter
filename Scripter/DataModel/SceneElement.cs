﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Scripter.DataModel
{
    /// <summary>
    /// Base class for scene elements. All other scene element types like
    /// Dialogs, Action texts and camera drive instructions are inherited
    /// from this class.
    /// </summary>
    public class SceneElement
    {
        public enum ElemType
        {
            SETypeDialog,
            SETypePerson,
            SETypeAction,
            SETypeShot,
            SETypeTransition,
            SETypeUnspecified
        }


        private ElemType mType = ElemType.SETypeUnspecified;


        /// <summary>
        /// Constructor of SceneElement base class.
        /// </summary>
        /// <param name="t">Type of the scene element that is being constructor.</param>
        public SceneElement(ElemType t)
        {
            mType = t;
        }

        /// <summary>
        /// Returns the scene element contents as one unformatted string.
        /// There can be newline characters that operate as separator
        /// character. This text could be placed to a single textbox
        /// for editing.
        /// </summary>
        /// <returns>Scene element's textual contents as one string.</returns>
        public virtual string GetAsString()
        {
            return "";
        }


        /// <summary>
        /// Returns the type of this scene element.
        /// </summary>
        /// <returns>Returns the type as enumeration.</returns>
        public virtual ElemType GetElementType()
        {
            return mType;
        }


        public virtual string GetSerialized()
        {
            return "";
        }

    }
}
