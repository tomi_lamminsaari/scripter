﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Scripter.DataModel
{
    /// <summary>
    /// SceneAction class operates as data structure for Action-parts
    /// of scene text.
    /// </summary>
    public class SceneElementAction : SceneElement
    {
        private string mText = "";


        /// <summary>
        /// ActionText property is a string that contains the action text.
        /// </summary>
        public string Text
        {
            get { return mText; }
            set { mText = value; }
        }

        public SceneElementAction() : base(ElemType.SETypeAction)
        {
        }

        public override string GetAsString()
        {
            return mText;
        }


        public void AppendText(string text)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(mText);
            builder.Append("\n");
            builder.Append(text);
            mText = builder.ToString();
        }
    }
}
