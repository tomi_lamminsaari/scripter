﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Scripter.DataModel.SceneElement;

namespace Scripter.DataModel
{
    public class SceneElementDialog : SceneElement
    {
        private string mText = "";

        
        public string Text
        {
            get { return mText; }
            set { mText = value; }
        }

        public SceneElementDialog() : base(ElemType.SETypeDialog)
        {

        }

        public SceneElementDialog(string text) : base(ElemType.SETypeDialog)
        {
            mText = text;
        }

        public override string GetAsString()
        {
            return mText;
        }
    }
}
