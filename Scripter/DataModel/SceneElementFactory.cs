﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.DataModel
{
    class SceneElementFactory
    {

        public static SceneElement CreateElement(SceneElement.ElemType t)
        {
            SceneElement elem = null;
            switch (t)
            {
                case SceneElement.ElemType.SETypeAction:
                    elem = new SceneElementAction();
                    break;
                case SceneElement.ElemType.SETypeDialog:
                    elem = new SceneElementDialog();
                    break;
                case SceneElement.ElemType.SETypeShot:
                    elem = new SceneElementShot();
                    break;
                case SceneElement.ElemType.SETypePerson:
                    elem = new SceneElementPerson();
                    break;
                default:
                    elem = null;
                    break;
            }
            return elem;
        }
    }
}
