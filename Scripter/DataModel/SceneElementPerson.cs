﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.DataModel
{
    public class SceneElementPerson : SceneElement
    {
        private string mPersonName = "";
        private string mPictureName = "";


        public string Text
        {
            get { return mPersonName; }
            set { mPersonName = value; }
        }

        public string PictureName
        {
            get { return mPictureName; }
            set { mPictureName = value; }
        }


        public SceneElementPerson() : base(SceneElement.ElemType.SETypePerson)
        {
        }

        public SceneElementPerson(string personName) : base(SceneElement.ElemType.SETypePerson)
        {
            mPersonName = personName;
        }

        public override string GetAsString()
        {
            return mPersonName;
        }
    }
}
