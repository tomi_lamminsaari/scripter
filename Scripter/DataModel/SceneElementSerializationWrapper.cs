﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Scripter.DataModel
{
    /// <summary>
    /// SceneElementSerializationWrapper is an utility c
    /// </summary>
    public class SceneElementSerializationWrapper
    {

        private Type mType;
        private string mData;

        public string SceneElementData
        {
            get { return mData; }
            set { mData = value; }
        }

        public Type SceneElementType
        {
            get { return mType; }
            set { mType = value; }
        }

        public SceneElementSerializationWrapper()
        {

        }

        public void FromSceneElement(SceneElement element)
        {
            if (element as SceneElementAction != null)
            {
                SerializeActionElement(element as SceneElementAction);
            }
            else if (element as SceneElementDialog != null)
            {
                SerializeDialogueElement(element as SceneElementDialog);
            }
            else if (element as SceneElementShot != null)
            {
                SerializeShotElement(element as SceneElementShot);
            }
        }

        public SceneElement ToSceneElement()
        {
            SceneElement elem = null;
            if (mType == typeof(SceneElementAction))
            {
                elem = new SceneElementAction();
            }
            else if (mType == typeof(SceneElementDialog))
            {
                elem = new SceneElementDialog();
            }
            else if (mType == typeof(SceneElementShot))
            {
                elem = new SceneElementShot();
            }
            return elem;
        }




        private void SerializeActionElement(SceneElementAction actionElement)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SceneElementAction));
            mData = null;
            using (StringWriter sww = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sww))
            {
                serializer.Serialize(writer, actionElement);
                mData = sww.ToString();
            }
        }

        private void SerializeDialogueElement(SceneElementDialog dialogueElement)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SceneElementDialog));
            mData = null;
            using (StringWriter sww = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sww))
            {
                serializer.Serialize(writer, dialogueElement);
                mData = sww.ToString();
            }
        }

        private void SerializeShotElement(SceneElementShot shotElement)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SceneElementShot));
            mData = null;
            using (StringWriter sww = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sww))
            {
                serializer.Serialize(writer, shotElement);
                mData = sww.ToString();
            }
        }
    }
}
