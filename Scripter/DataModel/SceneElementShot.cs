﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.DataModel
{
    public class SceneElementShot : SceneElement
    {
        private string mText = "";
        private string mShotType = "";

        public SceneElementShot() : base(ElemType.SETypeShot)
        {

        }

        public SceneElementShot(string shotType, string shotText) : base(ElemType.SETypeShot)
        {
            mShotType = shotType;
            mShotType = shotText;
        }
            
    }
}
