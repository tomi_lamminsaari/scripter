﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.DataModel
{
    public class SceneHeading
    {
        public static string PRIMARY_LOCATION_INT = "INT.";
        public static string PRIMARY_LOCATION_EXT = "EXT.";
        public static string PRIMARY_LOCATION_EXTINT = "INT/EXT.";

        private string mIntExt = "";
        private string mLocation = "";
        private string mTimeOfDay = "";


        /// <summary>
        /// A property that describes if scene is happening interior or exterior
        /// environment.
        /// </summary>
        public string PrimaryLocation
        {
            get { return mIntExt; }
            set
            {
                bool setIntExt = false;
                if (value.CompareTo(PRIMARY_LOCATION_EXT) == 0 ||
                    value.CompareTo(PRIMARY_LOCATION_INT) == 0 ||
                    value.CompareTo(PRIMARY_LOCATION_EXTINT) == 0)
                {
                    setIntExt = true;

                }

                if (setIntExt)
                {
                    mIntExt = value;
                }
            }
        }
        
        /// <summary>
        /// A property that tells the location where the scene happens.
        /// </summary>
        public string Location
        {
            get { return mLocation; }
            set { mLocation = value; }
        }
        
        /// <summary>
        /// Property that tells the time of day.
        /// </summary>
        public string TimeOfDay
        {
            get { return mTimeOfDay; }
            set { mTimeOfDay = value; }
        }


        // ===================================================================
        // Methods
        // ===================================================================

        public SceneHeading()
        {

        }

        public Boolean HasTimeOfDay()
        {
            if (mTimeOfDay == null || mTimeOfDay.Length < 1)
            {
                return false;
            }
            return true;
        }
    }
}
