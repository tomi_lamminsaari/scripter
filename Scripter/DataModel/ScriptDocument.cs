﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Windows.System.Threading;

namespace Scripter.DataModel
{
    /// <summary>
    /// ScriptDocument class is the main container of the whole movie script document.
    /// It contains all the scenes and other script related information and provides
    /// the functions for getting data and for modifying the script document.
    /// </summary>
    public class ScriptDocument
    {
        public enum ScriptSaveFormat { FountainFormat, XmlFormat }

        private ObservableCollection<Scene> mScenes = new ObservableCollection<Scene>();
        private string mScriptId;
        private string mScriptTitle;
        private DateTime mScriptDate = DateTime.Today;
        private string mAuthors = "";
        private string mCopyright = "Copyright \u00A9 ";
        private Index.LocationIndex mSceneHeaderIndex = new Index.LocationIndex();
        private Index.PeopleIndex mPeopleIndex = new Index.PeopleIndex();
        private object mLockObject = new object();
        private bool mUnsavedChanges = true;
        private string mTargetFileName;
        private ScriptSaveFormat mTargetFileFormat = ScriptSaveFormat.FountainFormat;
        


        /// <summary>
        /// Date when this script was created.
        /// </summary>
        public DateTime ScriptDate
        {
            get { return mScriptDate; }
            set { mScriptDate = value; }
        }


        /// <summary>
        /// ID of this script.
        /// TODO: This should be removed!
        /// </summary>
        public string ScriptId
        {
            get { return mScriptId; }
            set { mScriptId = value; }
        }


        /// <summary>
        /// Name of this story.
        /// </summary>
        public string ScriptTitle
        {
            get { return mScriptTitle; }
            set { mScriptTitle = value; }
        }


        /// <summary>
        /// The authors of this movie script.
        /// </summary>
        public string Authors
        {
            get { return mAuthors; }
            set { mAuthors = value; }
        }


        /// <summary>
        /// Copyright text for this movie script.
        /// </summary>
        public string Copyright
        {
            get { return mCopyright; }
            set { mCopyright = value; }
        }


        /// <summary>
        /// List of scene objects that belong to this movie script.
        /// </summary>
        public List<Scene> ScenesList
        {
            get
            {
                List<Scene> scenesList = new List<Scene>();
                foreach(var s in mScenes)
                {
                    scenesList.Add(s);
                }
                return scenesList;
            }
            set
            {
                mScenes.Clear();
                foreach (var s in value)
                {
                    mScenes.Add(s);
                }
            }
        }

        /// <summary>
        /// The prefered save format for this movie script.
        /// </summary>
        public ScriptSaveFormat TargetSaveFormat
        {
            get { return mTargetFileFormat; }
            set { mTargetFileFormat = value; }
        }

        

        /// <summary>
        /// Constructor.
        /// </summary>
        public ScriptDocument()
        {
            mScenes.Add(Scene.CreateTheEndScene(this));
            RefreshSceneHeaderIndex();
        }

        /// <summary>
        /// Adds new scene at this script document. The new scene will
        /// become the last scene of the script.
        /// </summary>
        /// <returns>Returns reference to the new scene object.</returns>
        public Scene NewScene()
        {
            Scene s = NewSceneAtIndex(mScenes.Count - 1);

            return s;
        }


        /// <summary>
        /// Adds new scene to this script document at certain position.
        /// </summary>
        /// <param name="index">Index number where the new scene will be added.</param>
        /// <returns>Returns reference to the new scene object.</returns>
        public Scene NewSceneAtIndex(int index)
        {
            Debug.WriteLine("NewSceneAtIndex: " + index.ToString());
            if (index < 0 || index >= mScenes.Count)
            {
                // Out of range.
                return null;
            }

            // Update the numbers of later scenes.
            for (int i = index; i < mScenes.Count; ++i)
            {
                mScenes[i].IncOrDecNumber(1);
            }

            // Create new scene to dialog.
            Scene s = new Scene(this);
            s.SetSceneNumber(index + 1);
            mScenes.Insert(index, s);

            RefreshSceneHeaderIndex();
            return s;
        }


        /// <summary>
        /// Deletes the scene object at certain index number.
        /// </summary>
        /// <param name="index">Index of the scene object to be removed from
        /// this script document.</param>
        public void DeleteScene(int index)
        {
            if (index < 0 || index >= mScenes.Count)
            {
                // Invalid index.
                return;
            }

            mScenes.RemoveAt(index);
            for (int i = index; i < mScenes.Count; ++i)
            {
                mScenes[i].IncOrDecNumber(-1);
            }

            RefreshSceneHeaderIndex();
            
        }

        /// <summary>
        /// Deletes certain scene object.
        /// </summary>
        /// <param name="s">Reference to the scene that will be removed from
        /// this script document.</param>
        public void DeleteScene(Scene s)
        {
            int ind = mScenes.IndexOf(s);
            if (ind != -1)
            {
                DeleteScene(ind);
            }

            RefreshSceneHeaderIndex();
        }


        /// <summary>
        /// Adds new dialog to given scene.
        /// </summary>
        /// <param name="scene">The scene where to add the dialog.</param>
        /// <param name="personName">Name of the person to be added.</param>
        /// <returns>Reference to created dialog object.</returns>
        /*
        public SceneElementDialog AddDialog(Scene scene, string personName)
        {
            var personObject = AddNewPerson(personName);
            var d = scene.AddDialog(personName);
            this.AssociatePersonAndScene(personObject, scene);
            return d;
        }
        */

        /// <summary>
        /// Refreshes all the keyword indices.
        /// </summary>
        public void RefreshIndex()
        {
            RefreshSceneHeaderIndex();
            RefreshPeopleIndex();
        }

        /// <summary>
        /// Asynchronously refreshes all the scene and people indices
        /// that relate to this movie script.
        /// </summary>
        /// <returns>Error code. 0 if no errors.</returns>
        public async Task<int> RefreshIndexAsync()
        {
            return await Task.Run(() =>
            {
                RefreshSceneHeaderIndex();
                RefreshPeopleIndex();
                return 0;
            });
        }

        /// <summary>
        /// Creates the "The end" scene at the end of script document.
        /// </summary>
        public void CreateTheEndScene()
        {
            var scene = Scene.CreateTheEndScene(this);
            mScenes.Add(scene);
        }


        /// <summary>
        /// Sets the script document's ID.
        /// </summary>
        /// <param name="id">ID of this script document.</param>
        public void SetDocumentId(string id)
        {
            mScriptId = id;
        }

        /// <summary>
        /// Sets the title of this script document.
        /// </summary>
        /// <param name="title">The title text.</param>
        public void SetScriptTitle(string title)
        {
            mScriptTitle = title;
        }


        /// <summary>
        /// Returns the title of this script document.
        /// </summary>
        /// <returns>String containing the title. Could be null.</returns>
        public string GetScriptTitle()
        {
            return mScriptTitle;
        }


        

        public string GetDocumentId()
        {
            return mScriptId;
        }

        public string SaveToString()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ScriptDocument));
            string xmlData = null;
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            using (StringWriter sww = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sww, settings))
            {
                serializer.Serialize(writer, this);
                xmlData = sww.ToString();
            }
            return xmlData;
        }

        public ObservableCollection<Scene> GetSceneList()
        {
            return mScenes;
        }

        public List<string> GetSceneLocations()
        {
            return mSceneHeaderIndex.GetLocations();
        }

        public ObservableCollection<string> GetCharacterNameList()
        {
            return mPeopleIndex.PersonNameList;
        }

        public List<Scene> GetPersonsSceneList(SceneElementPerson person)
        {
            return mPeopleIndex.GetPersonsSceneList(person);
        }


        /// <summary>
        /// Searches the persons by name and returns the Person object instance
        /// if one is found.
        /// </summary>
        /// <param name="name">Name of the person to be searched for.</param>
        /// <returns>The found person instance. Null if there is no persons with
        /// given name.</returns>
        public SceneElementPerson FindPersonByName(string name)
        {
            return mPeopleIndex.FindPerson(name);
        }

        /// <summary>
        /// Adds new person to this movie script.
        /// </summary>
        /// <param name="name">Name of the person to be added.</param>
        /// <returns>Reference to added person instance. Null if there was
        /// an error adding the person.</returns>
        public SceneElementPerson AddNewPerson(string name)
        {
            return mPeopleIndex.AddCharacter(name);
        }


        public void AssociatePersonAndScene(SceneElementPerson person, Scene scene)
        {
            mPeopleIndex.AssociatePersonAndScene(person, scene);
        }


        public void DisassociatePersonAndScene(SceneElementPerson person, Scene scene)
        {
            mPeopleIndex.DisassociatePersonAndScene(person, scene);
        }

        public void RenamePerson(SceneElementPerson person, string newName)
        {
            mPeopleIndex.RenamePerson(person, newName);
            /*
            foreach (var scene in mScenes)
            {
                var sceneElementsList = scene.GetSceneElements();
                foreach (var elem in sceneElementsList)
                {
                    SceneElementDialog dialogElem = elem as SceneElementDialog;
                    if (dialogElem != null)
                    {
                        dialogElem.PersonName = newName.ToUpper().Trim();
                    }
                }
            }
//            RefreshPeopleIndex();
        */
        }


        public void SetHavingUnsavedChanges(bool hasChanged)
        {
            mUnsavedChanges = hasChanged;
        }

        public bool IsHavingUnsavedChanges()
        {
            return mUnsavedChanges;
        }


        public void SetTargetFileName(string fn)
        {
            mTargetFileName = fn;
        }

        public string GetTargetFileName()
        {
            return mTargetFileName;
        }


        internal void NotifySceneHeadingUpdated(Scene scene)
        {
            RefreshSceneHeaderIndex();
        }

        private void RefreshSceneHeaderIndex()
        {
            // TODO: This function gets called way too often during the script
            // loading operation. It clears and repopulates the whole scene
            // index after each scene has been loaded.
            mSceneHeaderIndex.ClearIndex();
            foreach (Scene s in mScenes)
            {
                if (s.TheEndFlag == false)
                {
                    string headTxt = s.GetSceneHeading().Location;
                    mSceneHeaderIndex.AddLocation(headTxt, s);
                }
            }
            
        }

        private void RefreshPeopleIndex()
        {
            mPeopleIndex.ClearIndex();
            foreach (Scene s in mScenes)
            {
                mPeopleIndex.AddCharactersFromScene(s);
            }
        }

    }
}
