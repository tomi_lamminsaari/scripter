﻿using Scripter.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Scripter.DataModel
{
    /// <summary>
    /// A class that provides parsing functionalities for detecting the scene
    /// element types from text contents. There are also functions for
    /// checking if textbox contains valid formatting given element types.
    /// </summary>
    class ScriptTextRecognizer
    {
        public const int DialogLinePartPerson = 0;
        public const int DialogLinePartParenthetical = 1;
        public const int DialogLinePartBody = 2;

        public const int SceneHeaderPartIntExt = 5;
        public const int SceneHeaderPartLocation = 6;
        public const int SceneHeaderPartTimeOfDay = 7;


        public const string HeaderPartTxtInt = "INT.";
        public const string HeaderPartTxtExt = "EXT.";
        public const string HeaderPartTxtIntExt = "INT/EXT.";
        public const string HeaderPartTxtExtInt = "EXT/INT.";



        public ScriptTextRecognizer()
        {

        }


        /// <summary>
        /// Parses the given text and tells if its formatting matches with given
        /// scene element type.
        /// </summary>
        /// <param name="textBoxText">User edited text from textbox control.</param>
        /// <param name="elemType">Type of scene element the text should be.</param>
        /// <returns>Returns true if formatting of the given text matches the given
        /// type. Returns false if it does not match.</returns>
        public bool ParseTextBoxContents(string textBoxText, SceneElement.ElemType elemType)
        {
            string txt = textBoxText.Trim();
            if (elemType == SceneElement.ElemType.SETypeAction)
            {
                // Action element should have uppercase and lowercase characters
                // in its first line.
                int nlpos = txt.IndexOf('\n');
                if (nlpos > 0)
                {
                    string firstLineTxt = txt.Substring(0, nlpos);
                    return HasBothCase(firstLineTxt);
                }
                return true;
            }
            else if (elemType == SceneElement.ElemType.SETypeDialog)
            {
                return ValidateDialogText(textBoxText);
            }
            else if (elemType == SceneElement.ElemType.SETypeUnspecified)
            {
                return true;
            }
            return true;
        }

        /// <summary>
        /// Attempts to detect the scene element type from the given text.
        /// </summary>
        /// <param name="textBoxText">User edited text from the textbox control.</param>
        /// <returns>Returns the type of scene element the given text probably is.</returns>
        public SceneElement.ElemType DetectSceneElemType(string textBoxText)
        {
            bool firstLineHasUppercase = false;

            SceneElement.ElemType retType = SceneElement.ElemType.SETypeUnspecified;
            string txt = textBoxText.Trim();
            string firstLineTxt = "";
            int newlinePos = txt.IndexOf('\n');
            if (newlinePos > 0)
            {
                firstLineTxt = txt.Substring(0, newlinePos);
                bool hasLowercase = firstLineTxt.Any(char.IsLower);
                if (hasLowercase == false)
                {
                    firstLineHasUppercase = true;
                }
            } else if (newlinePos < 0) {
                // No new line in given text. Check if whole text is uppercase.
                bool hasLowercase = textBoxText.Any(char.IsLower);
                if (hasLowercase == false) {
                    firstLineHasUppercase = true;
                }
            }

            // Make decision about the type.
            if (firstLineHasUppercase)
            {
                retType = SceneElement.ElemType.SETypePerson;
            }
            else
            {
                retType = SceneElement.ElemType.SETypeAction;
            }

            if (StringParseUtils.EnclosedWithChars(txt, '(', ')')) {
                // This a single line of text that is inside parenthesis.
                retType = SceneElement.ElemType.SETypeDialog;
            }

            return retType;
        }


        public string[] ParseDialogTextLines(string textBoxText)
        {
            string[] separators = { "\n" };
            string[] lines = textBoxText.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            return lines;
        }

        public Dictionary<int, string> ParseDialogText(string textBoxText)
        {
            // TODO: Obsolote function !!!

            string[] lines = textBoxText.Split('\n');
            List<string> lines2 = new List<string>();

            for (int i=0; i < lines.Length; ++i)
            {
                string l = lines[i].Trim();

                // Select only lines that have content.
                if (l.Length > 0)
                {
                    lines2.Add(l);
                }
            }

            var dict = new Dictionary<int, string>();
            var bodyBuilder = new StringBuilder();
            for (int i=0; i < lines2.Count; ++i)
            {
                if (i == 0)
                {
                    // First line is supposed to be the person name.
                    dict.Add(DialogLinePartPerson, lines2.ElementAt(i));
                }
                else if (i == 1)
                {
                    // Second line could be either parenthetical or dialog body text.
                    // Parenthetical is surrounded by parentheses.
                    string l = lines2.ElementAt(i);
                    int parInd1 = l.IndexOf('(');
                    int parInd2 = l.IndexOf(')');
                    if (parInd1 == 0 && parInd2 == l.Length - 1)
                    {
                        // Remove the parenthesis from the text.
                        dict.Add(DialogLinePartParenthetical, l.Substring(1, l.Length - 2));
                    }
                    else
                    {
                        // No parenthetical.
                        bodyBuilder.Append(l);
                        bodyBuilder.Append("\n");
                    }
                }
                else
                {
                    // Lines with bigger index number are always the body text.
                    bodyBuilder.Append(lines2.ElementAt(i));
                    bodyBuilder.Append("\n");
                }
            }

            dict.Add(DialogLinePartBody, bodyBuilder.ToString());
            return dict;
        }


        public string GetParsedIntExt(string textboxValue)
        {
            string ret = HeaderPartTxtInt;

            string textboxValue2 = textboxValue.ToUpper();
            int i1 = textboxValue2.IndexOf("INT");
            int i2 = textboxValue2.IndexOf("EXT");
            if (i1 >= 0 && i2 >= 0)
            {
                if (i1 < i2)
                {
                    ret = HeaderPartTxtIntExt;
                }
                else
                {
                    ret = HeaderPartTxtExtInt;
                }
            }
            else if (i1 >= 0 && i2 < 0)
            {
                ret = HeaderPartTxtInt;
            }
            else if (i2 >= 0 && i1 < 0)
            {
                ret = HeaderPartTxtExt;
            }
            else
            {
                Debug.WriteLine("Parse error! No INT or EXT");
                ret = HeaderPartTxtInt;
            }
            return ret;
        }

        public Dictionary<int, string> GetParsedSceneHeader(string sceneHeaderLine, out bool err)
        {
            err = false;
            Dictionary<int, string> dict = new Dictionary<int, string>();
            try
            {
                // Find separator between INT. and location parts.
                string trimmedStr = sceneHeaderLine.Trim();
                string upperStr = trimmedStr.ToUpper();

                string intExtPart = "";
                string locPart = "";
                string todPart = "";
                string locationAndTODPart = ExtractSceneHeaderIntExtPart(upperStr, out intExtPart);

                if (locationAndTODPart.Length > 0)
                {
                    err = ExtractSceneHeaderLocationAndTOD(locationAndTODPart, out locPart, out todPart);
                }
                dict.Add(SceneHeaderPartIntExt, intExtPart);
                dict.Add(SceneHeaderPartLocation, locPart);
                dict.Add(SceneHeaderPartTimeOfDay, todPart);
            }
            catch (System.FormatException e)
            {
                dict.Clear();
                err = true;
            }
            
            return dict;
        }

        public bool EndsWith2Enters(TextBox textbox)
        {
            string tmpText = textbox.Text;
            if (tmpText != null && tmpText.Length > 3)
            {

                if (tmpText.ElementAt(tmpText.Length - 1) == '\n' && 
                    tmpText.ElementAt(tmpText.Length - 2) == '\r' &&
                    tmpText.ElementAt(tmpText.Length - 3) == '\n' &&
                    tmpText.ElementAt(tmpText.Length - 4) == '\r')
                {
                    // Two enters.
                    return true;
                }
            }
            return false;
        }


        public string ExtractSceneHeaderIntExtPart(string text, out string intExtPart)
        {
            if (text[0] == '.')
            {
                // This scene has only dot.
                // TODO: Return empty primary location.
            }

            char[] separatorChars = { '.', ' ' };
            string sub1 = "";
            string remainder = "";
            bool suc = StringParseUtils.NextTokenBySeparator(text, separatorChars, out sub1, out remainder);
            string sub2 = StringParseUtils.EnsureEndingCharacter(sub1, '.');

            if (suc == true)
            {
                if (sub2.CompareTo("INT.") == 0 ||
                    sub2.CompareTo("EXT.") == 0 ||
                    sub2.CompareTo("INT/EXT.") == 0 ||
                    sub2.CompareTo("EXT/INT.") == 0)
                {
                    intExtPart = sub2;
                    return remainder;
                }
            }
            intExtPart = "";
            return text;
        }


        public bool ExtractSceneHeaderLocationAndTOD(string text, out string loc, out string tod)
        {
            int i1 = text.IndexOf("-");
            if (i1 < 0)
            {
                // No time of day part.
                loc = text.Trim();
                tod = null;
                return false;
            }
            else
            {
                loc = text.Substring(0, i1).Trim();
                if (i1 < text.Length - 1)
                {
                    tod = text.Substring(i1 + 1).Trim();
                }
                else
                {
                    tod = "";
                }
                return false;
            }
            return true;
        }

        public static bool HasBothCase(string txt)
        {
            bool hasLC = txt.Any(Char.IsLower);
            bool hasUC = txt.Any(Char.IsUpper);
            return hasLC && hasUC;
        }

        public static bool IsUpperCase(string text)
        {
            bool hasLC = text.Any(Char.IsLower);
            return hasLC == false;
        }
        


        private bool ValidateDialogText(string text)
        {
            return false;
        }
    }
}
