﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.DataModel
{
    public class Transition : SceneElement
    {
        private string mTransitionText;


        public Transition() : base(ElemType.SETypeTransition)
        {
            mTransitionText = "";
        }

        //
        // Public properties
        //

        public string Text
        {
            get { return mTransitionText; }
            set
            {
                mTransitionText = value;
                if (mTransitionText == null)
                {
                    mTransitionText = "";
                }
            }
        }


        //
        // Public methods
        //

        public void SetText(string txt)
        {
            mTransitionText = txt;
        }


        public override ElemType GetElementType()
        {
            return ElemType.SETypeTransition;
        }
    }
}
