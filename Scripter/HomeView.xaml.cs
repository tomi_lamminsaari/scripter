﻿using Scripter.Utils;
using Scripter.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Scripter.DataModel;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Scripter
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HomeView : Page, DataModel.MovieScriptManager.IScriptScanObserver
    {
        private HomeViewModel mViewModel;

        public HomeView()
        {
            mViewModel = new HomeViewModel(this);
            this.InitializeComponent();
            this.DataContext = mViewModel;

            //var dbMan = Database.DatabaseManager.GetInstance();
            //dbMan.AddMovieScript(Database.DBUtils.CreateDocumentID(), "Kalevi", "NullFile");

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            mViewModel.RefreshRecentDocuments();
            IAppInterface app = App.GetCurrentApp() as IAppInterface;
            app.GetMovieScriptManager().RegisterScriptScanObserver(this);
            app.GetMovieScriptManager().ScanScripts();


            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            IAppInterface app = App.GetCurrentApp() as IAppInterface;
            app.GetMovieScriptManager().UnregisterScriptScanObserver(this);

            base.OnNavigatingFrom(e);
        }

        private void CreateNewDocument_Click(object sender, RoutedEventArgs e)
        {
            CreateNewDocumentAndOpenIt();
        }

        private async void OpenDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            // Load document.
            Windows.Storage.StorageFile file = await FileUtilities.OpenScriptLoadFile();

            // Load the movie script.
            if (file != null)
            {
                // Load the selected file and import it to the memory.
                string filePath = file.Path;
                string docId = await ImportMovieScriptFromFile(file);

                // Add the file to recent files list.
                IAppInterface app = App.GetCurrentApp() as IAppInterface;
                var docMan = app.GetDocumentManager();
                DataModel.ScriptDocument scriptDoc = docMan.FindScriptDocument(docId);
                mViewModel.AddToRecentDocuments(docId, scriptDoc.ScriptTitle);

                // Open the Script editing view.
                this.OpenScriptView(docId);
//                View.MovieScriptPage.CustomNavParams navParams = new View.MovieScriptPage.CustomNavParams();
//                navParams.ScriptDocumentId = docId;
//                this.Frame.Navigate(typeof(View.MovieScriptPage), navParams);
            }
            else
            {
                Debug.WriteLine("ERR! Failed to open file!");
                // TODO: Move to localized resources.
                MessageDialog msgBox = new MessageDialog("Failed to open movie script!");
                msgBox.Title = "File opening error!";
                await msgBox.ShowAsync();
            }

            
        }

        private void ScriptTitleName_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                CreateNewDocumentAndOpenIt();
                e.Handled = true;
            }
        }

        private void CreateNewDocumentAndOpenIt()
        {
            // Create new document.
            IAppInterface app = App.GetCurrentApp() as IAppInterface;
            var docMan = app.GetDocumentManager();
            string docId = DataModel.DocumentManager.CreateDocumentId();

            var doc = docMan.CreateNewDocument(docId);
            doc.SetScriptTitle(this.ScriptTitleName.Text);

            // Navigate to main script editing view.
            View.MovieScriptPage.CustomNavParams navParams = new View.MovieScriptPage.CustomNavParams();
            navParams.ScriptDocumentId = docId;
            this.Frame.Navigate(typeof(View.MovieScriptPage), navParams);
        }

        private async void RecentDocumentsListBox_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            int selectedRecentIndex = this.RecentDocumentsListBox.SelectedIndex;

            RecentDocumentListItem clickedItem = mViewModel.RecentDocumentFiles[selectedRecentIndex];

            string filename = clickedItem.FilePath;
#if false

            if (selectedRecentIndex >= 0 && selectedRecentIndex < mViewModel.RecentDocumentFiles.Count)
            {
                string filePath = mViewModel.RecentDocumentFiles.ElementAt(selectedRecentIndex).FilePath;
                Windows.Storage.StorageFile loadFile = await FileUtilities.OpenScriptLoadFileFromPath(filePath);
                if (loadFile != null)
                {
                    string scriptId = await ImportMovieScriptFromFile(loadFile);
                    this.OpenScriptView(scriptId);
                }
            }
#endif
            e.Handled = true;
        }


        /// <summary>
        /// Loads the movie script from given storage file and stores it to
        /// DocumentManager. It can be accessed by using the generated document id.
        /// </summary>
        /// <param name="fileToRead">The storage file from where to load the movie script.</param>
        /// <returns>The document if the loaded document.</returns>
        private async Task<string> ImportMovieScriptFromFile(Windows.Storage.StorageFile fileToRead)
        {
            // This imports the movie script to DocumentManager's data structures.
            IAppInterface app = App.GetCurrentApp() as IAppInterface;
            var docMan = app.GetDocumentManager();
            string docId = DataModel.DocumentManager.CreateDocumentId();
            var scriptDoc = docMan.CreateNewDocument(docId);
            var importer = DataModel.Converters.ScriptImporter.CreateImporter("fountain");
            await importer.ImportAsync(scriptDoc, fileToRead);

            return docId;
        }


        private void OpenScriptView(string scriptId)
        {
            View.MovieScriptPage.CustomNavParams navParams = new View.MovieScriptPage.CustomNavParams();
            navParams.ScriptDocumentId = scriptId;
            this.Frame.Navigate(typeof(View.MovieScriptPage), navParams);
        }


        void MovieScriptManager.IScriptScanObserver.ScriptScanComplete()
        {
            // The movie scripts scan is complete. Update the UI but do it in UI thread.
            Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                List<string> files = new List<string>();
                mViewModel.UpdateScriptsList(files);
            });
        }
    }
}
