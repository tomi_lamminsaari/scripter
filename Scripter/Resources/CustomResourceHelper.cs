﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Scripter.Resources
{
    /// <summary>
    /// An utility class that can load custom resource dictionaries and
    /// return the data in desired format.
    /// </summary>
    public class CustomResourceHelper
    {
        private Dictionary<string, ResourceDictionary> mCustomResources = new Dictionary<string, ResourceDictionary>();
        private Dictionary<string, string> mProfilePicUris = new Dictionary<string, string>()
        {
            { "person1", "ms-appx:///Assets/ContactIcon.png" },
            { "person2", "ms-appx:///Assets/ContactIcon_01.png" },
            { "person3", "ms-appx:///Assets/ContactIcon_02.png" },
            { "person4", "ms-appx:///Assets/ContactIcon_03.png" },
            { "person5", "ms-appx:///Assets/ContactIcon_04.png" }
        };
        private const string DefaultContactIconPath = "ms-appx:///Assets/ContactIcon.png";
        private int mNextProfilePicNameIndex = 0;


        public CustomResourceHelper()
        {
            
        }

        /// <summary>
        /// Frees the resources. Unloads the resource data from memory.
        /// </summary>
        public void Clear()
        {
            mCustomResources.Clear();
        }


        /// <summary>
        /// Returns the resource dictionary matching the given URL.
        /// </summary>
        /// <param name="uri">The URL of the resource data to be returned.</param>
        /// <returns></returns>
        public ResourceDictionary GetResourceDictionary(string uri)
        {
            ResourceDictionary dict = null;
            bool ret = mCustomResources.TryGetValue(uri, out dict);
            if (!ret)
            {
                // Resource not loaded yet.
                dict = new ResourceDictionary();
                dict.Source = new Uri(uri);
                mCustomResources.Add(uri, dict);
            }
            return dict;
        }


        public Thickness GetMarginResource(string uri, string marginName)
        {
            ResourceDictionary dict = GetResourceDictionary(uri);
            if (dict != null)
            {
                object o;
                dict.TryGetValue(marginName, out o);
                return (Thickness)o;
            }
            else
            {
                return new Thickness(0.0);
            }
        }


        public Style GetStyle(string uri, string styleName)
        {
            ResourceDictionary dict = GetResourceDictionary(uri);
            if (dict != null)
            {
                object o;
                dict.TryGetValue(styleName, out o);
                return (Style)o;
            }
            else
            {
                return new Style();
            }
        }


        /// <summary>
        /// Returns the URI to default profile picture image asset that matches
        /// the given logical profile picture name.
        /// </summary>
        /// <param name="logicalImageName">Logical name of character's profile image.</param>
        /// <returns>Uri of the image asset of the given logical name.</returns>
        public string GetProfilePictureURI(string logicalImageName)
        {
            string picUri = null;
            mProfilePicUris.TryGetValue(logicalImageName, out picUri);
            if (picUri == null || picUri.Length == 0)
            {
                picUri = CustomResourceHelper.DefaultContactIconPath;
            }
            return picUri;
        }


        /// <summary>
        /// Returns the next available logical name for new character.
        /// Use GetProfilePictureURI-function to get the URI of the image asset.
        /// </summary>
        /// <returns>Logical name of the character's profile picture.</returns>
        public string GetNextLogicalProfilePicture()
        {
            var logicalNames = mProfilePicUris.Keys;
            string retVal = logicalNames.ElementAt(mNextProfilePicNameIndex++);
            if (mNextProfilePicNameIndex >= logicalNames.Count)
            {
                mNextProfilePicNameIndex = 0;
            }
            return retVal;
        }
    }
}
