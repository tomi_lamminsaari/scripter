﻿using Scripter.Controls;
using Scripter.DataModel;
using Scripter.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Scripter
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SceneEditingView2 : Page, ISceneElementHost
    {
        private class CreateSceneElementInfo
        {
            public int SceneIndex { get; set; }
            public CreateSceneElementInfo(int num)
            {
                this.SceneIndex = num;
            }
        }

        #region MemberVariables

        private ScriptTextRecognizer mRecognizer = new DataModel.ScriptTextRecognizer();
        private ScriptDocument mDocument = null;
        private Scene mScene = null;

        #endregion


        public SceneEditingView2()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // Take the input parameters.
            Tuple<string, Scene> inputParam = (Tuple<string, Scene>)e.Parameter;
            IAppInterface app = (IAppInterface)App.GetCurrentApp();
            mDocument = app.GetDocumentManager().GetActiveDocument();
            mScene = inputParam.Item2;

            // Add the scene elements to the UI.
            UpdateSceneContents();

            // Show back button.
            App.GetCurrentApp().ShowBackButton();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            // Sync the scene element contents from scene element editors to the scene elements
            // in the scene object.
            SyncSceneContentsFromEditors();

            // Navigate back to previous view.
            this.Frame.GoBack();

        }

        /// <summary>
        /// Updates the UI to match the contents of the Scene-object.
        /// </summary>
        private void UpdateSceneContents()
        {
            StackPanel sp = this.ContentLayout;
            sp.Children.Clear();

            IAppInterface app = App.GetCurrentApp();
            ScriptDocument scriptDoc = app.GetDocumentManager().GetActiveDocument();

            // Update the scene heading to the top of the page.
            string sceneHeading = mScene.CombinedHeaderText;
            this.SceneHeadingEditor.Text = sceneHeading;

            // Add scene elements to the vertical stack panel.
            // Iterate all the scene elements in scene that was passed in to the
            // OnNavigatedTo() function.
            List<SceneElement> sceneElementsList = mScene.GetSceneElements();
            int addCount = 0;
            foreach (SceneElement sceneElem in sceneElementsList) {
                UserControl sceneElemControl = CreateNewSceneElement(sceneElem.GetElementType());
                ISceneElementEditor sceneElemEditor = sceneElemControl as ISceneElementEditor;
                if (sceneElemEditor != null) {
                    // Set the content from scene element model object.
                    sceneElemEditor.SetContent(sceneElem.GetAsString());
                }
                sceneElemEditor.SetId(addCount++);
                this.ContentLayout.Children.Add(sceneElemControl);
            }

            // Check if we didn't add any scene elements.
            if (addCount == 0) {
                // Add empty editor if scene didn't contain any actual elements.
                Controls.SceneElementAction actionElement = new Controls.SceneElementAction();
                actionElement.SetContent("");
                actionElement.SetId(addCount++);
                actionElement.SetSceneElementHost(this);
                this.ContentLayout.Children.Add(actionElement);
                actionElement.Focus(FocusState.Keyboard);
            }
            
        }

        private void AddAboveSelectionButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AddBelowSelectionButton_Click(object sender, RoutedEventArgs e)
        {

        }

        public void SceneElementEditingEnded(ISceneElementEditor editedControl, EditingEndReason endReason)
        {
            // Editing of element ended.
            if (endReason == EditingEndReason.EditingEndsEnterKey) {
                // User pressed enter. Check if we should create new element or move focus to next scene
                // element.
                int index = editedControl.GetId();
                
                if (editedControl.GetContent().Length == 0) {
                    // Empty editor. Don't create new editor below this one.

                } else {
                    // Editor contains some textual content. Detect the type of scene element based
                    // on the textual contents of the editor.
                    Debug.Assert(index >= 0 && index < GetSceneElementsCount());

                    // Take the editor that preceeds us.
                    ISceneElementEditor previousEditor = GetSceneElementEditorByIndex(index - 1);
                    DataModel.SceneElement.ElemType previousType = DataModel.SceneElement.ElemType.SETypeUnspecified;
                    if (previousEditor != null) {
                        previousType = previousEditor.GetSceneElementType();
                    }

                    // Try to detect the type of scene element the current editor is based on the
                    // textual data it contains.
                    DataModel.SceneElement.ElemType detectedType = mRecognizer.DetectSceneElemType(editedControl.GetContent());
                    ISceneElementEditor currentEditor = editedControl;

                    // Now we have the following information:
                    // - predefined type of the current editor
                    // - detected type of the current editor
                    // - type of the preceeding editor
                    // Based on this information we decide whether we should change the type of the current
                    // editor to something else than it was earlier expected to be.
                    bool changeEditorType = false;
                    DataModel.SceneElement.ElemType changeToType = DataModel.SceneElement.ElemType.SETypeUnspecified;
                    if (detectedType == DataModel.SceneElement.ElemType.SETypePerson) {
                        if (previousEditor != null && previousType == DataModel.SceneElement.ElemType.SETypePerson) {
                            // No two person-elements after one another.
                            changeToType = DataModel.SceneElement.ElemType.SETypeDialog;

                        } else {
                            // Change this editor to person.
                            changeToType = DataModel.SceneElement.ElemType.SETypePerson;
                        }
                        changeEditorType = true;

                    } else if (detectedType == DataModel.SceneElement.ElemType.SETypeAction) {
                        if (previousEditor != null) {
                            if (previousType == DataModel.SceneElement.ElemType.SETypePerson) {
                                // Person element above. This must be dialog.
                                changeToType = DataModel.SceneElement.ElemType.SETypeDialog;
                            } else if (previousType == DataModel.SceneElement.ElemType.SETypeDialog) {
                                // Dialog above. This is either action or second dialog.
                                if (StringParseUtils.EnclosedWithChars(previousEditor.GetContent(), '(', ')')) {
                                    // Previous dialog element contains text enclosed with parenthesis. It is 
                                    // a dialog hint. This element must be dialog as well.
                                    changeToType = SceneElement.ElemType.SETypeDialog;

                                } else {
                                    // Not a dialog inside parenthesis, like "(whispers)". Assume an action element.
                                    changeToType = DataModel.SceneElement.ElemType.SETypeAction;
                                }
                                
                            } else {
                                changeToType = DataModel.SceneElement.ElemType.SETypeAction;
                            }
                        } else {
                            // No previous element. This must be action.
                            changeToType = DataModel.SceneElement.ElemType.SETypeAction;
                        }
                        changeEditorType = true;

                    } else if (detectedType == SceneElement.ElemType.SETypeDialog) {
                        changeToType = SceneElement.ElemType.SETypeDialog;
                        changeEditorType = true;
                    }

                    // Change the type of the current editor if the decision making logic above decided that the editor type
                    // should be changed.
                    if (changeEditorType) {
                        UserControl newEditor = ChangeTypeOfSceneElementEditor(editedControl as UserControl, changeToType);
                        currentEditor = newEditor as ISceneElementEditor;
                    }

                    // Deduce the next scene element type and create new scene element to the view.
                    DataModel.SceneElement.ElemType elemType = DeduceNextSceneElementType(currentEditor);
                    UserControl newElement = CreateNewSceneElement(elemType);
                    AddSceneElementToLayout(newElement, index);
                    newElement.Focus(FocusState.Keyboard);
                }
            } else if (endReason == EditingEndReason.EditingEndsFocusPrevious || 
                       endReason == EditingEndReason.EditingEndsFocusNext) {
                // End editing of current scene element. Move focus to scene element that is above
                // this one or below this one.
                int currentIndex = editedControl.GetId();
                int nextEditorIndexOffset = endReason == EditingEndReason.EditingEndsFocusPrevious ? -1 : 1;
                ISceneElementEditor nextEditor = GetSceneElementEditorByIndex(currentIndex + nextEditorIndexOffset);
                if (nextEditor == null) {
                    // No other editors either above or below current editor. In that case
                    // we don't move the focus away from current editor.
                    return;

                } else {
                    int focusHint = (nextEditorIndexOffset < 0) ? ControlConstants.FocusLineLast : ControlConstants.FocusLineFirst;
                    nextEditor.SetEditorFocused(focusHint);
                }
                Debug.WriteLine("End editing! " + endReason.ToString());
            }
        }

        /// <summary>
        /// Attemps to deduce the next scene element type that would most likely follow the given
        /// scene element type.
        /// </summary>
        /// <param name="preceedingEditor">The editor that preceeds the new scene element.</param>
        /// <returns>Type of the scene element that most likely will follow the preceedingEditor</returns>
        private DataModel.SceneElement.ElemType DeduceNextSceneElementType(ISceneElementEditor preceedingEditor)
        {
            DataModel.SceneElement.ElemType elemType = preceedingEditor.GetSceneElementType();
            if (elemType == DataModel.SceneElement.ElemType.SETypeAction) {
                return DataModel.SceneElement.ElemType.SETypeAction;

            } else if (elemType == DataModel.SceneElement.ElemType.SETypePerson) {
                return DataModel.SceneElement.ElemType.SETypeDialog;

            } else if (elemType == DataModel.SceneElement.ElemType.SETypeDialog) {
                return DataModel.SceneElement.ElemType.SETypePerson;

            } else {
                return DataModel.SceneElement.ElemType.SETypeAction;
            }
        }

        /// <summary>
        /// Adds the given UI control to scene element content layout in the UI.
        /// </summary>
        /// <param name="sceneElement">The new scene element editor control.</param>
        /// <param name="addIndex">Index where in the layout the new control will be inserted.</param>
        /// <param name="addAfterIndex">Defines whether the new element will be added before or after
        /// the scene element editor in given 'addIndex' index.</param>
        private void AddSceneElementToLayout(UIElement sceneElement, int addIndex, bool addAfterIndex = true)
        {
            if (addAfterIndex) {
                // Add after the given index.
                this.ContentLayout.Children.Insert(addIndex + 1, sceneElement);

            } else {
                // Add before the given index.
                this.ContentLayout.Children.Insert(addIndex, sceneElement);
            }
            
            this.ContentLayout.InvalidateArrange();

            // Refresh the ID number of the scene element editors that belong to the
            // layout.
            int newId = 0;
            foreach (UIElement elem in this.ContentLayout.Children) {
                ISceneElementEditor sceneElem = elem as ISceneElementEditor;
                if (sceneElem != null) {
                    sceneElem.SetId(newId);
                }
                newId++;
            }
        }

        /// <summary>
        /// Changes the type of scene element editor.
        /// </summary>
        /// <param name="elementToChange">Current scene element editor.</param>
        /// <param name="newType">New type of the scene element editor that should replace the
        /// old one in UI.</param>
        /// <returns>The scene element editor that is in the UI. Could be new instance or the old
        /// one if new type and old type were the same.</returns>
        private UserControl ChangeTypeOfSceneElementEditor(UserControl elementToChange, DataModel.SceneElement.ElemType newType)
        {
            ISceneElementEditor oldEditor = elementToChange as ISceneElementEditor;
            if (oldEditor.GetSceneElementType() == newType) {
                // Old scene element is same type as the new. No need to create new scene element.
                return elementToChange;
            }
            int index = this.ContentLayout.Children.IndexOf(elementToChange);
            if (index >= 0 && index < GetSceneElementsCount()) {
                // Remove the old element from the UI.
                this.ContentLayout.Children.RemoveAt(index);

                // Create new editor for the scene element and copy the textual contents
                // from old editor to new editor.
                UserControl newElement = CreateNewSceneElement(newType);
                ISceneElementEditor newEditor = newElement as ISceneElementEditor;
                newEditor.SetContent(oldEditor.GetContent());
                AddSceneElementToLayout(newElement, index);
                return newElement;
            }
            return elementToChange;
        }

        /// <summary>
        /// Creates new scene element editor UI control.
        /// </summary>
        /// <param name="elemType"></param>
        /// <returns></returns>
        private UserControl CreateNewSceneElement(DataModel.SceneElement.ElemType elemType)
        {
            UserControl elem = null;
            if (elemType == DataModel.SceneElement.ElemType.SETypeAction) {
                var actionElem = new Controls.SceneElementAction();
                actionElem.SetContent("");
                actionElem.SetSceneElementHost(this);
                elem = actionElem;
            } else if (elemType == DataModel.SceneElement.ElemType.SETypeDialog) {
                var dialogElem = new Controls.SceneElementDialog();
                dialogElem.SetContent("");
                dialogElem.SetSceneElementHost(this);
                elem = dialogElem;

            } else if (elemType == DataModel.SceneElement.ElemType.SETypePerson) {
                var personElem = new Controls.SceneElementPerson();
                personElem.SetContent("");
                personElem.SetSceneElementHost(this);
                elem = personElem;

            } else if (elemType == DataModel.SceneElement.ElemType.SETypeShot) {

            } else if (elemType == DataModel.SceneElement.ElemType.SETypeTransition) {

            }
            return elem;
        }
        
        /// <summary>
        /// Returns the number of scene elements the current UI has.
        /// </summary>
        /// <returns>Number of scene elements the current UI has.</returns>
        private int GetSceneElementsCount()
        {
            return this.ContentLayout.Children.Count;
        }

        /// <summary>
        /// Returns the scene element editor instance by using its index number inside the
        /// editor layout.
        /// </summary>
        /// <param name="index">Index of the editor to return.</param>
        /// <returns>Reference to scene element editor or null or there is no editor in given
        /// index.</returns>
        private ISceneElementEditor GetSceneElementEditorByIndex(int index)
        {
            // Search for ISceneElementEditor instance from layout that has the given
            // index number is ID.
            foreach (UIElement uiElement in this.ContentLayout.Children) {
                ISceneElementEditor editor = uiElement as ISceneElementEditor;
                if (editor != null) {
                    if (editor.GetId() == index) {
                        return editor;
                    }
                }
            }
            return null;
        }

        private void SyncSceneContentsFromEditors()
        {
            if (mScene == null) {
                // No scene reference.
                return;
            }

            mScene.ClearSceneElements();
            

            // Copy the scene heading from editor to the Scene object.
            string headingText = this.SceneHeadingEditor.Text;
            bool err = false;
            Dictionary<int, string> headingDictionary = mRecognizer.GetParsedSceneHeader(headingText, out err);

            string intExtStr = "";
            string locStr = "";
            string todStr = "";
            headingDictionary.TryGetValue(ScriptTextRecognizer.SceneHeaderPartIntExt, out intExtStr);
            headingDictionary.TryGetValue(ScriptTextRecognizer.SceneHeaderPartLocation, out locStr);
            headingDictionary.TryGetValue(ScriptTextRecognizer.SceneHeaderPartTimeOfDay, out todStr);
            mScene.SetHeadingText(intExtStr, locStr, todStr);

            // Go through all the editors in the layout and transfer their data to the the
            // scene element objects in scene.
            foreach (UIElement editorUiElement in this.ContentLayout.Children) {
                ISceneElementEditor editor = editorUiElement as ISceneElementEditor;
                if (editor != null) {
                    SceneElement sceElem = SceneElementFactory.CreateElement(editor.GetSceneElementType());
                    if (sceElem as DataModel.SceneElementAction != null ) {
                        DataModel.SceneElementAction actionElem = sceElem as DataModel.SceneElementAction;
                        actionElem.Text = editor.GetContent();

                    } else if (sceElem as DataModel.SceneElementDialog != null) {
                        DataModel.SceneElementDialog dialogElemet = sceElem as DataModel.SceneElementDialog;
                        dialogElemet.Text = editor.GetContent();

                    } else if (sceElem as DataModel.SceneElementPerson != null) {
                        DataModel.SceneElementPerson personElement = sceElem as DataModel.SceneElementPerson;
                        personElement.Text = editor.GetContent();
                    }

                    if (sceElem != null) {
                        mScene.AddSceneElement(sceElem);
                    }
                }
            }
        }
    }
}
