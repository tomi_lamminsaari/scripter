﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.Utils
{
    class FileUtilities
    {

        public static async Task<Windows.Storage.StorageFile> OpenScriptSaveFile(string suggestedFileName)
        {
            Windows.Storage.StorageFile saveFile;
            if (suggestedFileName == null || suggestedFileName.Length == 0)
            {
                // Show file picker for picking the save file.
                var picker = new Windows.Storage.Pickers.FileSavePicker();
                picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
                picker.FileTypeChoices.Add("Movie Scripts", new List<string>() { ".xml" });
                saveFile = await picker.PickSaveFileAsync();
            }
            else
            {
                // Attempt to open save file to the path defines by the suggested file name.
                saveFile = await Windows.Storage.StorageFile.GetFileFromPathAsync(suggestedFileName);
            }

            return saveFile;
        }

        public static async Task<Windows.Storage.StorageFile> OpenScriptSaveFileWithOptions(string suggestedFileName, string fileExtension)
        {
            Windows.Storage.StorageFile saveFile;
            if (suggestedFileName == null || suggestedFileName.Length == 0)
            {
                // Show file picker.
                var picker = new Windows.Storage.Pickers.FileSavePicker();
                picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
                picker.FileTypeChoices.Add("Fountain Scripts", new List<string>() { fileExtension });
                saveFile = await picker.PickSaveFileAsync();
            }
            else
            {
                saveFile = await Windows.Storage.StorageFile.GetFileFromPathAsync(suggestedFileName);
            }
            return saveFile;
        }


        /// <summary>
        /// Creates a save file to application's private directory. The given script ID operates
        /// as a base for the filename.
        /// </summary>
        /// <param name="scriptId">The script ID that operates as a base for the file name.</param>
        /// <returns>Task for file creation.</returns>
        public static async Task<Windows.Storage.StorageFile> OpenPrivateScriptSaveFile(
                string scriptId, string fileExtension)
        {
            var storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            string scriptFileName = scriptId + fileExtension;
            Windows.Storage.StorageFile saveFile = await storageFolder.CreateFileAsync(scriptFileName);
            return saveFile;
        }


        public static async Task<Windows.Storage.StorageFile> OpenScriptLoadFile()
        {
            
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
            picker.FileTypeFilter.Add(".fountain");
            Windows.Storage.StorageFile loadFile = await picker.PickSingleFileAsync();
            return loadFile;
        }


        public static async Task<Windows.Storage.StorageFile> OpenScriptLoadFileFromPath(string filePath)
        {
            Windows.Storage.StorageFile loadFile = null;
            loadFile = await Windows.Storage.StorageFile.GetFileFromPathAsync(filePath);
            return loadFile;
        }
    }
}
