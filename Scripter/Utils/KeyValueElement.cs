﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.Utils
{
    public class KeyValueElement
    {
        private string mName = "";
        private object mValue;

        public string Name
        {
            get { return mName; }
            set { mName = value; }
        }

        public object Value
        {
            get { return mValue; }
            set { mValue = value; }
        }

        public KeyValueElement()
        {

        }

        public KeyValueElement(string name)
        {
            mName = name;
        }

        public KeyValueElement(string name, string value)
        {
            mName = name;
            mValue = value;
        }

        public string AsString()
        {
            return (string)mValue;
        }
    }
}
