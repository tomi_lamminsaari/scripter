﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.Utils
{
    /// <summary>
    /// KeyValueNode extends KeyValueElement class and it works as
    /// a collection of KeyValueElement objects.
    /// </summary>
    public class KeyValueNode : KeyValueElement
    {
        private List<KeyValueElement> mChildren = new List<KeyValueElement>();


        /// <summary>
        /// Returns the number of children KeyValueElements this node has.
        /// </summary>
        public int ChildCount
        {
            get { return mChildren.Count; }
        }


        /// <summary>
        /// Default constructor.
        /// </summary>
        public KeyValueNode()
        {

        }


        /// <summary>
        /// Constructor that sets the name for this node element.
        /// </summary>
        /// <param name="name">Name of this element.</param>
        public KeyValueNode(string name) : base(name)
        {
        }


        /// <summary>
        /// Constructor that sets the name and value for this node element.
        /// </summary>
        /// <param name="name">Name of this element.</param>
        /// <param name="value">Value of this element.</param>
        public KeyValueNode(string name, string value) : base(name, value)
        {

        }


        /// <summary>
        /// Adds child KeyValueElement to this node.
        /// </summary>
        /// <param name="child">The child element to add. Must not be added earlier.</param>
        public void AddChild(KeyValueElement child)
        {
            mChildren.Add(child);
        }


        /// <summary>
        /// Removes the child KeyValueElement that has the given name.
        /// </summary>
        /// <param name="name">Name of the child element to be removed.</param>
        public void RemoveChild(string name)
        {
            var childElem = FindFirstChild(name);
            if (childElem != null)
            {
                RemoveChild(childElem);
            }
        }


        /// <summary>
        /// Removes the child element.
        /// </summary>
        /// <param name="childElement">The child KeyValueElement object to be removed.</param>
        public void RemoveChild(KeyValueElement childElement)
        {
            mChildren.Remove(childElement);
        }


        /// <summary>
        /// Returns child element based on the index number.
        /// </summary>
        /// <param name="index">Index of the child to be returned.</param>
        /// <returns>The child object.</returns>
        public KeyValueElement GetChildByIndex(int index)
        {
            return mChildren.ElementAt(index);
        }


        /// <summary>
        /// Searches for first child element with given name.
        /// </summary>
        /// <param name="name">Name of the child element to look for.</param>
        /// <returns>The child element.</returns>
        public KeyValueElement FindFirstChild(string name)
        {
            foreach(var c in mChildren)
            {
                if (string.Compare(c.Name, name) == 0)
                {
                    return c;
                }
            }
            return null;
        }


        /// <summary>
        /// Returns all the child elements that have given name.
        /// </summary>
        /// <param name="name">Name of the children to be returned.</param>
        /// <returns>List of the KeyValueElement objects. An empty list will be
        /// returned if no children are not found.</returns>
        public List<KeyValueElement> FindChildren(string name)
        {
            List<KeyValueElement> childrenList = new List<KeyValueElement>();
            foreach(var c in mChildren)
            {
                if (string.Compare(c.Name, name) == 0)
                {
                    childrenList.Add(c);
                }
            }
            return childrenList;
        }
    }
}
