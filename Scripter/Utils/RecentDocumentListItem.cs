﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.Utils
{
    /// <summary>
    /// A class that represents a single movie script file that has been stored to the
    /// list of recently accessed movie script files.
    /// </summary>
    public class RecentDocumentListItem
    {
        public string FilePath { get; set; }
        public string ScriptTitle { get; set; }

        public RecentDocumentListItem()
        {
            this.FilePath = "";
            this.ScriptTitle = "";
        }

        public RecentDocumentListItem(string path, string title)
        {
            this.FilePath = path;
            this.ScriptTitle = title;
        }

        public string GetAsString()
        {
            return String.Format("{0}:{1}", this.FilePath, this.ScriptTitle);
        }

        public void ParseFromString(string recentDataParts)
        {
            string[] parts = recentDataParts.Split(':');
            if (parts.Length == 0)
            {
                throw new FormatException("RecentDocumentListItem input data is not proper!");
            }
            else if (parts.Length == 1)
            {
                this.FilePath = parts[0];
                this.ScriptTitle = "Unnamed";
            }
            else
            {
                this.FilePath = parts[0];
                this.ScriptTitle = parts[1];
            }

        }

        public override string ToString()
        {
            return this.ScriptTitle;
        }
    }
}
