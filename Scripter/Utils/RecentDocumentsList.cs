﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Scripter.Utils
{

    /// <summary>
    /// List that keeps track on recently accessed movie scripts.
    /// </summary>
    public class RecentDocumentsList
    {
        private List<RecentDocumentListItem> mRecentScripts = new List<RecentDocumentListItem>();
        private const int MAX_ENTRIES = 10;
        private const string RECENT_SETTINGNAME_PREFIX = "recent_";

        /// <summary>
        /// Adds the given script id to the recent scripts list.
        /// </summary>
        /// <param name="scriptId">ID of the script to be added.</param>
        /// <returns>Returns true if successful.</returns>
        public static bool AddRecentDocument(string filePath, string scriptTitle)
        {
            var recents = new RecentDocumentsList();
            if (recents.LoadRecentFilesList() == false)
            {
                // Loading failed.
                recents.ClearRecentsList();
            }
            recents.MarkFileAccessed(filePath, scriptTitle);
            recents.SaveRecentFilesList();
            return true;
        }

        public static List<RecentDocumentListItem> GetRecentDocuments()
        {
            List<RecentDocumentListItem> returnedList = null;
            var recents = new RecentDocumentsList();
            if (recents.LoadRecentFilesList() == false)
            {
                // Loading failed. Return an empty list.
                returnedList = new List<RecentDocumentListItem>();
            }
            else
            {
                returnedList = recents.RecentFilesList;
            }
            return returnedList;
        }


        /// <summary>
        /// Constructor.
        /// </summary>
        RecentDocumentsList()
        {

        }
        
        /// <summary>
        /// Adds the given script id to recent documents list.
        /// </summary>
        /// <param name="filePath">Path to the file to be added.</param>
        void MarkFileAccessed(string filePath, string scriptTitle)
        {
            // Add the filePath to the beginning of the recent files list.
            mRecentScripts.Insert(0, new RecentDocumentListItem(filePath, scriptTitle));

            // If there are too many entries we remove the entries from the
            // end.
            while (mRecentScripts.Count >= MAX_ENTRIES)
            { 
                mRecentScripts.RemoveAt(mRecentScripts.Count - 1);
            }
        }

        
        /// <summary>
        /// Property that contains the recent files list.
        /// </summary>
        List<RecentDocumentListItem> RecentFilesList
        {
            get { return mRecentScripts; }
        }

        /// <summary>
        /// Removes the given script ID from the list.
        /// </summary>
        /// <param name="scriptId">ID of the script to be removed.</param>
        void RemoveScriptFromList(string scriptId)
        {

        }

        bool SaveRecentFilesList()
        {
            // Get the local settings object and write the recent file paths
            // to the local settings.
            var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
            for (int i=0; i < MAX_ENTRIES; ++i)
            {
                string saveString = "";
                if (i < mRecentScripts.Count)
                {
                    saveString = mRecentScripts[i].GetAsString();
                }

                // Each recent file entry will be written as their own setting name.
                // The name will consist of static part and then increasing index number.
                var settingName = String.Format("{0}{1}", RECENT_SETTINGNAME_PREFIX, i);
                settings.Values[settingName] = saveString;
            }
            return true;
        }

        bool LoadRecentFilesList()
        {
            mRecentScripts.Clear();
            var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
            for (int i=0; i < MAX_ENTRIES; ++i)
            {
                try
                {
                    var settingName = String.Format("{0}{1}", RECENT_SETTINGNAME_PREFIX, i);
                    Object obj = settings.Values[settingName];
                    if (obj != null)
                    {
                        var filePathStr = obj as string;
                        if (filePathStr != null && filePathStr.Length > 0)
                        {
                            var recentItem = new RecentDocumentListItem();
                            recentItem.ParseFromString(filePathStr);
                            mRecentScripts.Add(recentItem);
                        }
                    }
                }
                catch (Exception e)
                {
                    // We just quietly ignore the exception.
                }
            }
            return true;
        }


        void ClearRecentsList()
        {
            mRecentScripts.Clear();
        }

    }
}
