﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.Utils
{
    /// <summary>
    /// StringParseUtils class contains some useful functions for parsing
    /// strings.
    /// </summary>
    class StringParseUtils
    {

        /// <summary>
        /// Tells if given string contains a colon somewhere.
        /// </summary>
        /// <param name="txt">The string to be checked.</param>
        /// <returns>Returns true if there is a colon. False is there is not.</returns>
        public static bool HasColon(string txt)
        {
            bool ret = false;
            if (txt.IndexOf(':') < 0)
            {
                ret = false;
            }
            else
            {
                ret = true;
            }
            return ret;
        }


        /// <summary>
        /// Tells if given string begins with indentation. The amount of indentation can
        /// be given.
        /// </summary>
        /// <param name="txt">The string to be checked.</param>
        /// <param name="indentAmount">Number of spaces in the beginning of string are
        /// considered to be an indentation.</param>
        /// <returns>Returns true if string begins either with given number of spaces or
        /// if line begins with tab-symbol.</returns>
        public static bool BeginsWithIndent(string txt, int indentAmount=3)
        {
            bool ret = false;
            string indentStr = new string(' ', indentAmount);
            if (txt.IndexOf(indentStr) == 0 || txt.IndexOf('\t') == 0)
            {
                ret = true;
            }
            else
            {
                ret = false;
            }

            return ret;
        }


        /// <summary>
        /// Tells if given string is an empty string. Empty means that its length is
        /// zero or it contains only whitespace characters.
        /// </summary>
        /// <param name="txt">The string to be checked.</param>
        /// <returns>Returns true if string is an empty string.</returns>
        public static bool IsEmptyString(string txt)
        {
            string trimmed = txt.Trim();
            return trimmed.Length == 0;
        }


        /// <summary>
        /// Tells if given string contains only upper case characters.
        /// </summary>
        /// <param name="text">The string to be analyzed.</param>
        /// <returns>Returns true if only upper case characters are met.</returns>
        public static bool OnlyUpperCaseCharacters(string text)
        {
            foreach (char c in text)
            {
                if (Char.IsLower(c))
                {
                    return false;
                }
            }
            return true;
        }


        /// <summary>
        /// Tells if given string begins with given character and ends with
        /// another given character.
        /// </summary>
        /// <param name="text">The string to be processed.</param>
        /// <param name="begChar">The beginning character.</param>
        /// <param name="endChar">The ending character.</param>
        /// <returns></returns>
        public static bool EnclosedWithChars(string text, char begChar, char endChar)
        {
            if (text != null || text.Length > 2)
            {
                if (text[0] == begChar && text[text.Length - 1] == endChar)
                {
                    return true;
                }
            }
            return false;
        }


        /// <summary>
        /// Parses the given string and splits the lines to a array of strings.
        /// </summary>
        /// <param name="text">The string to be processed.</param>
        /// <returns>An array of string parsed from the given string by splitting
        /// it from line change characters.</returns>
        public static string[] SplitLines(string text)
        {
            string[] separators = { "\n" };
            string[] ret = text.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            if (ret != null)
            {
                for (int i = 0; i < ret.Length; ++i)
                {
                    ret[i] = ret[i].Trim();
                }
            }
            return ret;
        }


        /// <summary>
        /// Returns the next section of the given string. The separator character is one of the
        /// characters from given separators array.
        /// </summary>
        /// <param name="inputString"></param>
        /// <param name="separators"></param>
        /// <param name="subStr"></param>
        /// <param name="remainingString"></param>
        /// <returns></returns>
        public static bool NextTokenBySeparator(string inputString, char[] separators, out string subStr, out string remainingString)
        {
            subStr = null;
            remainingString = inputString;


            if (inputString == null)
            {
                return false;
            }
            for (int i=0; i < inputString.Length; ++i)
            {
                char currentChar = inputString[i];
                bool isSeparatorChar = false;
                foreach (char c in separators)
                {
                    if (currentChar == c)
                    {
                        isSeparatorChar = true;
                        break;
                    }
                }
                if (isSeparatorChar)
                {
                    // Current character is a separator character. We can cut the inputString
                    // from this location.
                    subStr = inputString.Substring(0, i);
                    remainingString = inputString.Substring(i+1);
                    return true;
                }
            }

            // None of the separators were found. We return error but copy
            // whole string to be the token and remainder will be an empty
            // string.
            subStr = inputString;
            remainingString = "";
            return false;
        }


        /// <summary>
        /// Makes sure the given string ends with given character.
        /// </summary>
        /// <param name="inputString">The string to be checked.</param>
        /// <param name="endChar">The ending character. If string ends with this, the original
        /// string will be returned. If string does not end with this character, this character
        /// gets appended to the input string.</param>
        /// <returns>The original string plus given endChar unless the string actually ends with
        /// that character.</returns>
        public static string EnsureEndingCharacter(string inputString, char endChar)
        {
            if (inputString.Length == 0)
            {
                return endChar.ToString();
            }
            if (inputString[inputString.Length - 1] != endChar)
            {
                return inputString + endChar;
            }
            return inputString;
        }
    }
}

