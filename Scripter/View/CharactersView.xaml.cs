﻿using Scripter.DataModel;
using Scripter.Utils;
using Scripter.View.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Scripter.View
{
    public sealed partial class CharactersView : UserControl, ICustomSplitViewContent
    {
        IParentPageHelper mHelper;
        private bool mSkipNameChangedEvent = false;

        private CharactersViewModel mViewModel;



        public CharactersView(IParentPageHelper helper)
        {
            mHelper = helper;
            mViewModel = new CharactersViewModel(this);
            this.InitializeComponent();
            this.DataContext = mViewModel;

        }

        public void RefreshCustomContent(Dictionary<string, object> newPageParams)
        {
            mViewModel.RefreshViewContents();
        }

        public void HandleNavigateFromEvent(PageStateEntry entry, Windows.UI.Xaml.Navigation.NavigationEventArgs args)
        {

        }

        public void HandleNavigateToEvent(PageStateEntry entry, Windows.UI.Xaml.Navigation.NavigationEventArgs args)
        {

        }

        public void CharactersListBox_CharacterItemSelected(object sender, SelectionChangedEventArgs e)
        {
            object selObject = this.CharactersListBox.SelectedItem;
            if (selObject != null)
            {
                string characterName = (string)selObject;
                Debug.WriteLine("Character item selected: " + characterName);
                UpdateCharacterToUI(characterName);
            }
        }

        public IParentPageHelper GetParentViewHelper()
        {
            return mHelper;
        }


        private void UpdateCharacterToUI(string personName)
        {
            var doc = mHelper.GetActiveDocument();
            var person = doc.FindPersonByName(personName);
            mViewModel.ShowPersonDetails(person);
        }

        private async void FlyoutCharacterNameTextBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                string characterName = this.FlyoutCharacterNameTextBox.Text.Trim().ToUpper();
                var doc = mHelper.GetActiveDocument();
                var person = doc.AddNewPerson(characterName);
                if (person != null)
                {
                    // Person added. Update the persons list.
                    var resourceHelper = App.GetCurrentApp().GetCustomResources();
                    person.PictureName = resourceHelper.GetNextLogicalProfilePicture();
                    Debug.WriteLine("Picture name: " + person.PictureName);
                }
                else
                {
                    // TODO: An error occured!
                    Debug.WriteLine("CharactersView: Adding new person failed!");
                    // TODO: Move error message to localized resources.
//                    MessageDialog msgBox = new MessageDialog("Failed to add! Person name already in use!");
//                    msgBox.Title = "Adding person failed!";
//                    await msgBox.ShowAsync();
                }

                // Hide flyout
                this.FlyoutCharacterNameTextBox.Text = "";
                this.CharacterNameFlyout.Hide();
                e.Handled = true;
            }
        }


        private async void DeletePersonButton_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Move to localized resources.
            string sceneCountStr = mViewModel.AppearsSceneCount;

            string msg = String.Format("Really want to delete {0}? The person appears in {1} scenes." +
                "All the dialogs he or she has will be deleted as well.", 
                mViewModel.SelectedCharacterName,
                sceneCountStr);

            MessageDialog msgBox = new MessageDialog(msg);
            const int YESACTION = 0;
            const int NOACTION = 1;
            msgBox.Commands.Add(new Windows.UI.Popups.UICommand("Yes") { Id = YESACTION });
            msgBox.Commands.Add(new Windows.UI.Popups.UICommand("No") { Id = NOACTION });
            msgBox.Title = "Delete person";
            var result = await msgBox.ShowAsync();
            int cmdId = (int)result.Id;
            if (cmdId == YESACTION)
            {
                Debug.WriteLine("Yes pressed!");
            }
            else
            {
                Debug.WriteLine("No pressed!");
            }
            
        }

        private void CharacterNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (mSkipNameChangedEvent)
            {
                mSkipNameChangedEvent = false;
                return;
            }

            // When user start to edit the person name, we show the Apply-button.
            if (mViewModel.IsShowingRealPerson)
            {
                string personName = this.CharacterNameTextBox.Text;
                var tmpPerson = mHelper.GetActiveDocument().FindPersonByName(personName);
                
            }
            
        }

        private void SetPersonEditingState(bool isEditing)
        {
            mViewModel.EditModeEnabled = isEditing;
            if (isEditing)
            {
                this.CharacterNameTextBox.IsReadOnly = false;
                this.CharacterHistoryTextBox.IsReadOnly = false;
                this.DeletePersonButton.Visibility = Visibility.Collapsed;
                this.CancelEditButton.Visibility = Visibility.Visible;
                this.CharacterNameTextBox.Visibility = Visibility.Visible;
                this.CharacterNameTextBlock.Visibility = Visibility.Collapsed;
                this.CharacterHistoryTextBox.Visibility = Visibility.Visible;
                this.CharacterStoryTextBlock.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.CharacterNameTextBox.IsReadOnly = true;
                this.CharacterHistoryTextBox.IsReadOnly = true;
                this.DeletePersonButton.Visibility = Visibility.Visible;
                this.CancelEditButton.Visibility = Visibility.Collapsed;
                this.CharacterNameTextBox.Visibility = Visibility.Collapsed;
                this.CharacterNameTextBlock.Visibility = Visibility.Visible;
                this.CharacterHistoryTextBox.Visibility = Visibility.Collapsed;
                this.CharacterStoryTextBlock.Visibility = Visibility.Visible;
            }
        }

        private void EditApplyButton_Click(object sender, RoutedEventArgs e)
        {
            // Save the changes from text editors to the person object.
            if (mViewModel.EditModeEnabled)
            {
                SetPersonEditingState(false);
                string personName = this.CharacterNameTextBox.Text.Trim().ToUpper();
                string personStory = this.CharacterHistoryTextBox.Text;

                var doc = mHelper.GetActiveDocument();
                var person = doc.FindPersonByName(mViewModel.SelectedCharacterName);
                if (person != null)
                {
                    // Rename the person.
                    doc.RenamePerson(person, personName);
                }
                mViewModel.ShowPersonDetails(person);
            }
            else
            {
                SetPersonEditingState(true);
            }
        }

        private void CancelEditButton_Click(object sender, RoutedEventArgs e)
        {
            SetPersonEditingState(false);
            mViewModel.PersonEditingCancelled();
        }

        private void AppearInScenesList_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            int selectedIndex = this.AppearInScenesList.SelectedIndex;
            Scene doubleClickedScene = mViewModel.GetCurrentPersonSceneByIndex(selectedIndex);
            if (doubleClickedScene != null)
            {
                Debug.WriteLine("Scene doubleclicked! Index = {0}, Scene = {1}", selectedIndex, doubleClickedScene.Heading);

                // Create he Scenes view opening parameters that will scroll the view
                // to show certain scene in the list.
                var newPageParams = new Dictionary<string, object>();
                newPageParams.Add(ScenesView.SCROLL_TO_SCENE, doubleClickedScene);
                mHelper.OpenContentPage(typeof(ScenesView), newPageParams);
            }
            else
            {
                Debug.WriteLine("Scene doubleclicked! Scene was not found!");
            }
            e.Handled = true;
        }
    }
}
