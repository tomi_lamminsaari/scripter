﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.View
{
    public class CompactSceneItem : INotifyPropertyChanged
    {
        public int SceneIndex { get; set; }
        public string SceneNumber { get; set; }
        private string mIntExt = "";
        public string IntExt
        {
            get { return mIntExt; }
            set { mIntExt = value;  NotifyPropertyChanged("IntExt"); }
        }
        private string mSceneTitle;
        public string SceneTitle
        {
            get { return mSceneTitle; }
            set { mSceneTitle = value; NotifyPropertyChanged("SceneTitle"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
