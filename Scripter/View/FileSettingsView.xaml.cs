﻿using Scripter.Utils;
using Scripter.View.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Scripter.View
{
    public sealed partial class FileSettingsView : UserControl, INotifyPropertyChanged, ICustomSplitViewContent
    {
        private IParentPageHelper mHelper;
        private FileSettingsViewModel mViewModel;

        public FileSettingsView(IParentPageHelper helper)
        {
            mViewModel = new FileSettingsViewModel(this);
            this.DataContext = mViewModel;

            this.InitializeComponent();
            this.mHelper = helper;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void HandleNavigateFromEvent(PageStateEntry entry, NavigationEventArgs args)
        {
            throw new NotImplementedException();
        }

        public void HandleNavigateToEvent(PageStateEntry entry, NavigationEventArgs args)
        {
            if (entry != null)
            {
                int index = entry.GetIntValue("selectedSceneIndex", -1);
                //this.ScenesList.SelectedIndex = index;

                // There was a problem to get listview scrolled to correct item
                // by setting it in OnNavigateTo function. That's why we use
                // DispatchTimer to scroll it to selected item after a short
                // delay.
                //this.dispatchTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
                //this.dispatchTimer.Start();
                //this.NotifyPropertyChanged("ScenesList");
            }
        }

        public void RefreshCustomContent(Dictionary<string, object> newPageParams)
        {
            mViewModel.RefreshAllProperties();
        }


        public Scripter.DataModel.ScriptDocument GetActiveDocument()
        {
            return mHelper.GetActiveDocument();
        }




        private void SaveFormatFountainAction_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("File format for Fountain selected!");

            var doc = mHelper.GetActiveDocument();
            var currentFileName = doc.GetTargetFileName();
            mViewModel.SetTargetSaveFormat(DataModel.ScriptDocument.ScriptSaveFormat.FountainFormat);

            // Change the filename to end with ".fountain" extension.
            if (currentFileName != null && currentFileName.Length > 0)
            {
                int extensionIndex = currentFileName.LastIndexOf(".");
                if (extensionIndex >= 0)
                {
                    mViewModel.SetTargetFileName(currentFileName.Substring(0, extensionIndex) + ".fountain");
                }
            }
        }

        private void SaveFormatXmlAction_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("File format for XML selected!");
            var doc = mHelper.GetActiveDocument();
            var currentFileName = doc.GetTargetFileName();
            mViewModel.SetTargetSaveFormat(DataModel.ScriptDocument.ScriptSaveFormat.XmlFormat);

            // Change the filename to end with "xml"-extension.
            if (currentFileName != null && currentFileName.Length > 0)
            {
                int extensionIndex = currentFileName.LastIndexOf(".");
                if (extensionIndex >= 0)
                {
                    mViewModel.SetTargetFileName(currentFileName.Substring(0, extensionIndex) + ".xml");
                }
            }
        }

        private async void SaveAsButton_Click(object sender, RoutedEventArgs e)
        {
            // Show save as dialog.
            var doc = GetActiveDocument();
            string exporterType = "fountain";
            string fileFormatExtension = ".fountain";
            if (doc.TargetSaveFormat == DataModel.ScriptDocument.ScriptSaveFormat.FountainFormat)
            {
                fileFormatExtension = ".fountain";
                exporterType = "fountain";
            }
            else if (doc.TargetSaveFormat == DataModel.ScriptDocument.ScriptSaveFormat.XmlFormat)
            {
                fileFormatExtension = ".xml";
                exporterType = "xml";
            }
            var saveFile = await FileUtilities.OpenScriptSaveFileWithOptions(doc.GetTargetFileName(), fileFormatExtension);
            if (saveFile == null)
            {
                // No save file. User probably cancelled the saving.
                Debug.WriteLine("No saveFile instance created. User probably cancelled!");
            }
            else
            {
                string filename = saveFile.Path;
                mViewModel.SetTargetFileName(filename);

                // Create exporter instance. Then create also the data stream where the generated
                // save file will be written to.
                var exporter = Scripter.DataModel.Converters.ScriptExporter.CreateExporter(exporterType);
                using (Windows.Storage.Streams.IRandomAccessStream saveStream = await saveFile.OpenAsync(Windows.Storage.FileAccessMode.ReadWrite))
                {
                    using (Windows.Storage.Streams.DataWriter writer = new Windows.Storage.Streams.DataWriter(saveStream))
                    {
                        await exporter.Export(doc, writer);
                    }
                }
            }
        }
    }
}
