﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.View
{
    /// <summary>
    /// ICustomSplitViewContent is an interface the sub pages must implement to properly
    /// function as a sub page of MovieScriptPage.
    /// </summary>
    public interface ICustomSplitViewContent
    {
        /// <summary>
        /// This function will be called by owning parent page when it thinks the child
        /// page must refresh the contents in UI.
        /// </summary>
        /// <param name="viewParams">Dictionary for passing view specific extra parameters.
        /// Can be null.</param>
        void RefreshCustomContent(Dictionary<string, object> viewParams);

        /// <summary>
        /// Called by owning parent page when it receives OnNavigatedFrom event. In this
        /// function the current sub page can store its contents to given PageStateEntry
        /// instance so that page can be restored later.
        /// </summary>
        /// <param name="entry">The page state storage item where the page's current
        /// state should be stored for later restoration.</param>
        /// <param name="args">The navigation arguments from the runtime.</param>
        void HandleNavigateFromEvent(PageStateEntry entry, Windows.UI.Xaml.Navigation.NavigationEventArgs args);

        /// <summary>
        /// Called when owning parent page receives OnNavigatedTo event. In this function
        /// the sub page can restore its state to previous state.
        /// </summary>
        /// <param name="entry">The page state entry containing the earlier state of this page.
        /// This can be null if there is no earlier page entry.</param>
        /// <param name="args">The navigation arguments from the runtime.</param>
        void HandleNavigateToEvent(PageStateEntry entry, Windows.UI.Xaml.Navigation.NavigationEventArgs args);

    }
}
