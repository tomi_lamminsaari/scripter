﻿using Scripter.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Scripter.View
{
    /// <summary>
    /// The interface parent page implements. Contains set ow utility functions
    /// that allow the sub page to get access some of the general application
    /// environment services.
    /// </summary>
    public interface IParentPageHelper
    {
        /// <summary>
        /// Returns the reference to current movie script document that is being edited.
        /// </summary>
        /// <returns>The movie script instance that is being edited.</returns>
        Scripter.DataModel.ScriptDocument GetActiveDocument();

        /// <summary>
        /// Returns the document id of the currently active movie script document.
        /// </summary>
        /// <returns>Current movie script document id.</returns>
        string GetActiveDocumentId();

        /// <summary>
        /// Shows or hides the modal waiting animation.
        /// </summary>
        /// <param name="showing">Pass true to bring waiting animation visible. Pass false
        /// to hide it.</param>
        void ToggleWaitingAnimation(bool showing);
                
        /// <summary>
        /// Returns the window frame of current page.
        /// </summary>
        /// <returns>Reference to current window frame.</returns>
        Frame GetCurrentPageFrame();


        void OpenContentPage(Type subPageType, Dictionary<string, object> newPageParams);
    }
}
