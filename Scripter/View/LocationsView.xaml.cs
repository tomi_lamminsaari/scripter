﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Scripter.View
{
    public sealed partial class LocationsView : UserControl, INotifyPropertyChanged, ICustomSplitViewContent
    {

        private IParentPageHelper mHelper;
        private List<string> mLocationsList = new List<string>();

        public List<string> LocationListItemsSource
        {
            get
            {
                Debug.WriteLine("Get location list! Itemcount: " + mLocationsList.Count.ToString());
                return mLocationsList;
            }
        }


        public LocationsView(IParentPageHelper helper)
        {
            this.InitializeComponent();
            this.DataContext = this;
            mHelper = helper;
        }


        public void RefreshCustomContent(Dictionary<string, object> newPageParams)
        {
            var doc = mHelper.GetActiveDocument();
            mLocationsList = doc.GetSceneLocations();
            NotifyPropertyChanged("LocationListItemsSource");
        }

        public void HandleNavigateFromEvent(PageStateEntry entry, Windows.UI.Xaml.Navigation.NavigationEventArgs args)
        {

        }

        public void HandleNavigateToEvent(PageStateEntry entry, Windows.UI.Xaml.Navigation.NavigationEventArgs args)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }


        private void LocationListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

    }
}
