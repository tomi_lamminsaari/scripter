﻿using Scripter.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace Scripter.View.Models
{
    class CharactersViewModel : INotifyPropertyChanged
    {
        private CharactersView mParentView;
        private ObservableCollection<string> mCharactersItems = new ObservableCollection<string>();
        private ObservableCollection<string> mCharactersScenesList = new ObservableCollection<string>();
        private string mSelectedName = "";
        private string mSelectedStory = "";
        private string mSceneCountStr = "0";
        private bool mIsShowingRealPerson = false;
        private bool mEditModeEnabled = false;
        private string mPersonProfilePicPath = "";
        private SceneElementPerson mPersonToShow;


        public ObservableCollection<string> CharactersListBoxItemsSource
        {
            get
            {
                Debug.WriteLine("getter: CharactersListBoxItemsSource");
                return mParentView.GetParentViewHelper().GetActiveDocument().GetCharacterNameList();
            }
        }

        public string SelectedCharacterName
        {
            get { return mSelectedName; }
            set
            {
                mSelectedName = value;
                NotifyPropertyChanged("SelectedCharacterName");
            }
        }

        public string PersonProfilePicPath
        {
            get { return mPersonProfilePicPath; }
            set
            {
                mPersonProfilePicPath = value;
                NotifyPropertyChanged("PersonProfilePicPath");
            }
        }

        public string SelectedCharacterStory
        {
            get { return mSelectedStory; }
            set
            {
                mSelectedStory = value;
                NotifyPropertyChanged("SelectedCharacterStory");
            }
        }

        public string AppearsSceneCount
        {
            get { return mSceneCountStr; }
            set
            {
                mSceneCountStr = value;
                NotifyPropertyChanged("AppearsSceneCount");
            }
        }

        public bool IsShowingRealPerson
        {
            get { return mIsShowingRealPerson; }
            set
            {
                mIsShowingRealPerson = value;
                NotifyPropertyChanged("IsShowingRealPerson");
            }
        }

        public string EditApplyLabel
        {
            get
            {
                if (mEditModeEnabled)
                {
                    // Returns checkmark.
                    return "\xE8FB";
                }
                else
                {
                    // Return edit icon.
                    return "\xE70F";
                }
            }
        }

        public bool EditModeEnabled
        {
            get { return mEditModeEnabled; }
            set
            {
                mEditModeEnabled = value;
                NotifyPropertyChanged("EditModeEnabled");
                NotifyPropertyChanged("EditApplyLabel");
            }
        }

        public SceneElementPerson PersonToShow
        {
            get { return mPersonToShow; }
            set
            {
                mPersonToShow = value;
                NotifyPropertyChanged("PersonToShow");
            }
        }

        public ObservableCollection<string> CharactersScenesList
        {
            get { return mCharactersScenesList; }
            set
            {
                mCharactersScenesList = value;
                NotifyPropertyChanged("CharactersScenesList");
            }
        }


        public CharactersViewModel(CharactersView parentView)
        {
            mParentView = parentView;
        }

        public void ShowPersonDetails(SceneElementPerson personToShow)
        {
            mPersonToShow = personToShow;
            if (personToShow == null)
            {
                // No person. Empty all the selected user info UI elements.
                this.SelectedCharacterName = "";
                this.SelectedCharacterStory = "";
                this.AppearsSceneCount = "0";
                this.IsShowingRealPerson = false;
            }
            else
            {
                /*
                var doc = mParentView.GetParentViewHelper().GetActiveDocument();
                this.AppearsSceneCount = String.Format("{0}", doc.GetPersonsSceneList(personToShow).Count);
                this.SelectedCharacterName = personToShow.Name;
                this.SelectedCharacterStory = personToShow.Story;
                var resourceHelper = App.GetCurrentApp().GetCustomResources();
                string profilePicPath = resourceHelper.GetProfilePictureURI(personToShow.PictureName);
                if (profilePicPath == null)
                {
                    Debug.WriteLine("UpdateCharacterToUI(): ERROR! Null URI");
                }
                this.PersonProfilePicPath = profilePicPath;
                this.IsShowingRealPerson = true;

                // Update scenes list.
                ObservableCollection<string> personsScenesList = new ObservableCollection<string>();
                var perscenelist = doc.GetPersonsSceneList(personToShow);
                foreach (var s in perscenelist)
                {
                    personsScenesList.Add(s.GetSceneNumberAndHeading());
                }
                this.CharactersScenesList = personsScenesList;
                */
            }
        }

        public Scene GetCurrentPersonSceneByIndex(int sceneIndex)
        {
            if (mPersonToShow == null)
            {
                return null;
            }
            else
            {
                var doc = mParentView.GetParentViewHelper().GetActiveDocument();
                var charactersScenes = doc.GetPersonsSceneList(mPersonToShow);
                return charactersScenes.ElementAt(sceneIndex);
            }
            
        }

        public void PersonEditingCancelled()
        {
            NotifyPropertyChanged("SelectedCharacterName");
            NotifyPropertyChanged("SelectedCharacterStory");
        }

        public void RefreshViewContents()
        {
            mCharactersItems.Clear();
            var scriptDoc = mParentView.GetParentViewHelper().GetActiveDocument();
            mCharactersItems = scriptDoc.GetCharacterNameList();
            NotifyPropertyChanged("CharactersListBoxItemsSource");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
