﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.View.Models
{
    class FileSettingsViewModel : INotifyPropertyChanged
    {
        private FileSettingsView mParentView;


        public string CurrentScriptFileName
        {
            get
            {
                var doc = mParentView.GetActiveDocument();
                return doc.GetTargetFileName();
            }
        }

        public Boolean IsSaveButtonEnabled
        {
            get
            {
                var filename = this.CurrentScriptFileName;
                if (filename != null && filename.Length > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public Boolean IsFountainFormatChecked
        {
            get
            {
                if (mParentView.GetActiveDocument().TargetSaveFormat == DataModel.ScriptDocument.ScriptSaveFormat.FountainFormat)
                {
                    return true;
                }
                return false;
            }
        }

        public Boolean IsXmlFormatChecked
        {
            get
            {
                if (mParentView.GetActiveDocument().TargetSaveFormat == DataModel.ScriptDocument.ScriptSaveFormat.XmlFormat)
                {
                    return true;
                }
                return false;
            }
        }


        
        public FileSettingsViewModel(FileSettingsView parentView)
        {
            mParentView = parentView;
        }


        public void SetTargetFileName(string filename)
        {
            var doc = mParentView.GetActiveDocument();
            doc.SetTargetFileName(filename);
            NotifyPropertyChanged("CurrentScriptFileName");
            NotifyPropertyChanged("IsSaveButtonEnabled");
        }

        public void SetTargetSaveFormat(DataModel.ScriptDocument.ScriptSaveFormat saveFormat)
        {
            var doc = mParentView.GetActiveDocument().TargetSaveFormat = saveFormat;
            NotifyPropertyChanged("IsXmlFormatChecked");
            NotifyPropertyChanged("IsFountainFormatChecked");
        }


        public void RefreshAllProperties()
        {
            NotifyPropertyChanged("");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
