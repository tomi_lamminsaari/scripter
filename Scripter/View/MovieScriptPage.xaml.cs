﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Scripter.DataModel;
using Scripter.Controls;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using Windows.UI.Popups;
using Windows.UI.Core;
using Scripter.Utils;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Scripter.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MovieScriptPage : Page, IParentPageHelper, IScenePanelObserver
    {
        public class CustomNavParams
        {
            public string ScriptDocumentId { get; set; }
        }


        private string mDocumentId;
        private ICustomSplitViewContent mCurrentSubPage;
        private Type mSubViewType;


        public MovieScriptPage()
        {
            this.InitializeComponent();

            this.KeyDown += MovieScriptPage_KeyDown;

        }

        private void MovieScriptPage_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            var controlKeyState = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.Control);
            if (e.Key == Windows.System.VirtualKey.S && controlKeyState != CoreVirtualKeyStates.None)
            {
                // Control S pressed down in this window. 
                e.Handled = true;
            }
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs args)
        {
            base.OnNavigatedTo(args);

            CustomNavParams customNavParams = args.Parameter as CustomNavParams;
            if (customNavParams != null)
            {
                mDocumentId = customNavParams.ScriptDocumentId;
            }


            // Get navigation parameters. These usually contain information
            // about the Scrip Document that is open.

            // Check the page stack for this view. If entry exists, it means
            // that we are comming back to this page and we should restore
            // the previous page state.
            PageStateStorage pageStorage = App.GetCurrentApp().GetPageStateStorage();
            PageStateEntry pageEntry = pageStorage.GetAndPopPageStateEntry(typeof(MovieScriptPage).ToString());
            if (pageEntry != null)
            {
                // Based on the page type that was found from history we
                // create the same kind of content view again.
                mSubViewType = pageEntry.GetTypeValue("subPageType");
                UIElement contentView = GetContentViewObject(mSubViewType);
                this.ScriptSplitView.Content = contentView;
                mCurrentSubPage = (ICustomSplitViewContent)contentView;
                
            }
            else
            {
                // This page is not in page stack. Open ScenesView by default.
                UIElement contentView = GetContentViewObject(typeof(ScenesView));
                mSubViewType = typeof(ScenesView);
                this.ScriptSplitView.Content = contentView;
                mCurrentSubPage = (ICustomSplitViewContent)contentView;
            }

            mCurrentSubPage.HandleNavigateToEvent(pageEntry, args);
            mCurrentSubPage.RefreshCustomContent(null);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            // Save the page state only when navigating forward. Don't save
            // on back-navigation.
            if (e.NavigationMode != NavigationMode.Back)
            {
                // Store the current UI state so that we can restore it later.
                PageStateStorage pageStorage = App.GetCurrentApp().GetPageStateStorage();
                PageStateEntry pageEntry = pageStorage.CreatePageStateEntry(typeof(MovieScriptPage).ToString());
                pageEntry.SetValue("subPageType", mSubViewType);

                if (mCurrentSubPage != null)
                {
                    mCurrentSubPage.HandleNavigateFromEvent(pageEntry, e);
                }
            }
            
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            this.ScriptSplitView.IsPaneOpen = !this.ScriptSplitView.IsPaneOpen;
            ((RadioButton)sender).IsChecked = false;
        }

        private void FileMenuButton_Click(object sender, RoutedEventArgs e)
        {
            this.ScriptSplitView.IsPaneOpen = false;
            ShowContentView(new FileSettingsView(this), typeof(FileSettingsView), null);
        }

        private void ScenesButton_Click(object sender, RoutedEventArgs e)
        {
            this.ScriptSplitView.IsPaneOpen = false;
            
            ShowContentView(new ScenesView(this), typeof(ScenesView), null);
        }

        private async void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            ScriptDocument scriptDoc = GetActiveDocument();
            bool doNavigateBack = false;
            if (scriptDoc.IsHavingUnsavedChanges())
            {
                // Script document has unsaved changes. Ask if user wants to
                // save the document before closing.
                // TODO: Localize texts.
                var dial = new Windows.UI.Popups.MessageDialog("Current document has not been saved. Do you want to save it before closing it?");
                dial.Title = "Unsaved changes";
                dial.Commands.Add(new UICommand("Save", new UICommandInvokedHandler(CommandHandlers)));
                //dial.Commands.Add(new UICommand("Close", new UICommandInvokedHandler(CommandHandlers)));
                dial.Commands.Add(new UICommand("Close", null));
                var res = await dial.ShowAsync();

                doNavigateBack = true;
            }

            if (doNavigateBack && this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }

        private async void CommandHandlers(IUICommand commandLabel)
        {
//            DataModel.Converters.ScriptExporter exporter = new DataModel.Converters.ScriptExporter();
//            await exporter.Export();
        }
            

        private void CharactersButton_Click(object sender, RoutedEventArgs e)
        {
            this.ScriptSplitView.IsPaneOpen = false;

            ShowContentView(new CharactersView(this), typeof(CharactersView), null);
        }

        private void PlacesButton_Click(object sender, RoutedEventArgs e)
        {
            this.ScriptSplitView.IsPaneOpen = false;

            ShowContentView(new LocationsView(this), typeof(LocationsView), null);
        }

        private void ShowContentView(UserControl contentView, Type contentViewType, Dictionary<string, object> newPageParams)
        {
            mSubViewType = contentViewType;
            this.ScriptSplitView.Content = contentView;
            mCurrentSubPage = contentView as ICustomSplitViewContent;
            mCurrentSubPage.RefreshCustomContent(newPageParams);
        }

        /// <summary>
        /// Implementation of IParentPageHelper.GetActiveDocument().
        /// </summary>
        /// <returns></returns>
        public ScriptDocument GetActiveDocument()
        {
            IAppInterface appInterface = App.GetCurrentApp();
            return appInterface.GetDocumentManager().GetActiveDocument();
        }

        /// <summary>
        /// Implementation of IParentPageHelper.GetActiveDocumentId().
        /// </summary>
        /// <returns></returns>
        public string GetActiveDocumentId()
        {
            return mDocumentId;
        }

        /// <summary>
        /// Implementation of IParentPageHelper.ToggleWaitingAnimation().
        /// </summary>
        /// <param name="showing"></param>
        public void ToggleWaitingAnimation(bool showing)
        {
            if (showing)
            {
                this.WaitingAnimation.IsActive = true;
                this.WaitingAnimation.IsEnabled = true;
                this.ModalAnimView.Visibility = Visibility.Visible;
            }
            else
            {
                this.WaitingAnimation.IsActive = false;
                this.WaitingAnimation.IsEnabled = false;
                this.ModalAnimView.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Implementation of IParentPageHelper.GetCurrentPageFrame().
        /// </summary>
        /// <returns></returns>
        public Frame GetCurrentPageFrame()
        {
            return (Frame)Window.Current.Content;
        }

        /// <summary>
        /// Implementation of IParentPageHelper.OpenContentPage().
        /// </summary>
        /// <param name="pageType"></param>
        /// <param name="newPageParams"></param>
        public void OpenContentPage(Type pageType, Dictionary<string, object> newPageParams)
        {
            // Create the content page and add it to SplitView's content area.
            UserControl contentView = GetContentViewObject(pageType);
            ShowContentView(contentView, pageType, newPageParams);

            // Set the SplitView's selected menu button to match the new
            // content page.
            this.ToggleSplitViewMenuButton(pageType);
        }


        public async void SaveCurrentDocument()
        {
            IAppInterface appInterface = App.GetCurrentApp();
            var scriptDoc = appInterface.GetDocumentManager().GetActiveDocument();
            string xmlData = scriptDoc.SaveToString();

            if (xmlData != null && xmlData.Length > 0)
            {
                var picker = new Windows.Storage.Pickers.FileSavePicker();
                picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
                picker.FileTypeChoices.Add("Movie Scripts", new List<string>() { ".fountain" });
                Windows.Storage.StorageFile file = await picker.PickSaveFileAsync();
                if (file != null)
                {
                    Debug.WriteLine("Write document to file!");
                    Windows.Storage.CachedFileManager.DeferUpdates(file);
                    await Windows.Storage.FileIO.WriteTextAsync(file, xmlData);
                    Windows.Storage.Provider.FileUpdateStatus status = await Windows.Storage.CachedFileManager.CompleteUpdatesAsync(file);
                    if (status == Windows.Storage.Provider.FileUpdateStatus.Complete)
                    {
                        Debug.WriteLine("Movie Script saved to a file.");
                    }
                    else
                    {
                        // TODO: Location
                        var dialog = new Windows.UI.Popups.MessageDialog("Could not write to the file for some reason.");
                        dialog.Title = "Saving failed!";
                        await dialog.ShowAsync();
                    }
                }
            }
        }

        /// <summary>
        /// Implementation of IScenePanelObserver
        /// </summary>
        /// <param name="refScene"></param>
        /// <param name="scenePos"></param>
        public void CreateNewScene(Scene refScene, SceneInsertionPos scenePos)
        {
            bool insertBefore = true;
            if (scenePos == SceneInsertionPos.AfterRefScene)
            {
                insertBefore = false;
            }

            int sceneIndex = -1;
            var doc = this.GetActiveDocument();
            if (refScene == null)
            {
                // No reference scene. This must be the first scene in the Script.
                doc.NewScene();
            }
            else
            {
                if (insertBefore)
                {
                    // Add scene before reference scene.
                    sceneIndex = refScene.GetSceneNumber() - 1;
                    doc.NewSceneAtIndex(sceneIndex);
                }
                else
                {
                    // Add scene after reference scene.
                    sceneIndex = refScene.GetSceneNumber();
                    doc.NewSceneAtIndex(sceneIndex);
                }
            }
            ScenesView scenesView = this.ScriptSplitView.Content as ScenesView;
            if (scenesView != null)
            {
                scenesView.RefreshCustomContent(null);
                if (sceneIndex != -1)
                {
                    scenesView.SetSelectedSceneIndex(sceneIndex);
                }
            }
        }

        /// <summary>
        /// Implementation of IScenePanelObserver
        /// </summary>
        /// <param name="scene"></param>
        public void OpenSceneEditor(Scene scene)
        {
            IAppInterface appInterface = App.GetCurrentApp();

            int mOpenedSceneIndex = scene.GetSceneNumber();
            var doc = appInterface.GetDocumentManager().GetActiveDocument();
            Tuple<string, Scene> sceneEditParams = new Tuple<string, Scene>(mDocumentId, scene);
            this.Frame.Navigate(typeof(SceneEditingView2), sceneEditParams);

        }


        private UserControl GetContentViewObject(Type pageType)
        {
            UserControl uiObject = null;
            if (pageType == typeof(FileSettingsView))
            {
                uiObject = new FileSettingsView(this);
            }
            else if (pageType == typeof(ScenesView))
            {
                uiObject = new ScenesView(this);
            }
            else if (pageType == typeof(CharactersView))
            {
                uiObject = new CharactersView(this);
            }
            return uiObject;
        }

        private void ToggleSplitViewMenuButton(Type contentPageType)
        {
            if (contentPageType == typeof(FileSettingsView))
            {
                this.FileMenuButton.IsChecked = true;
            }
            else if (contentPageType == typeof(ScenesView))
            {
                this.ScenesButton.IsChecked = true;
            }
            else if (contentPageType == typeof(CharactersView))
            {
                this.CharactersButton.IsChecked = true;
            }
            else if (contentPageType == typeof(LocationsView))
            {
                this.PlacesButton.IsChecked = true;
            }

        }
    }
}
