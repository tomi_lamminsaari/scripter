﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.View
{
    public class PageStateEntry
    {

        private string mPageName = "";
        private Dictionary<string, object> mValues = new Dictionary<string, object>();


        public PageStateEntry(string pageName)
        {
            mPageName = pageName;
        }


        public void SetValue(string key, object val)
        {
            mValues.Add(key, val);
        }


        public object GetValue(string key)
        {
            object result = null;
            mValues.TryGetValue(key, out result);
            if (result == null)
            {
                return null;
            }
            else
            {
                return result;
            }
        }

        public string GetStringValue(string key)
        {
            object obj = GetValue(key);
            if (obj != null)
            {
                return obj as string;
            }
            else
            {
                return null;
            }
        }

        public int GetIntValue(string key, int defaultVal)
        {
            object obj = GetValue(key);
            if (obj != null)
            {
                return (int)obj;
            }
            else
            {
                return defaultVal;
            }
        }

        public Type GetTypeValue(string key)
        {
            object obj = GetValue(key);
            if (obj != null)
            {
                return obj as Type;
            }
            return null;
        }

    }
}
