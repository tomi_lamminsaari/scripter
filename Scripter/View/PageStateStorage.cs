﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.View
{
    public class PageStateStorage
    {

        private Dictionary<string, PageStateEntry> mPageEntries = new Dictionary<string, PageStateEntry>();

        public PageStateStorage()
        {

        }


        public PageStateEntry CreatePageStateEntry(string pageName)
        {
            PageStateEntry entry = GetPageStateEntry(pageName);
            if (entry == null)
            {
                entry = new PageStateEntry(pageName);
                mPageEntries.Add(pageName, entry);
            }
            return entry;
        }

        public PageStateEntry GetPageStateEntry(string pageName)
        {
            PageStateEntry entry = null;
            mPageEntries.TryGetValue(pageName, out entry);
            return entry;
        }


        public PageStateEntry GetAndPopPageStateEntry(string pageName)
        {
            PageStateEntry entry = GetPageStateEntry(pageName);
            ClearPageStateEntry(pageName);
            return entry;
        }


        public void ClearPageStateEntry(string pageName)
        {
            PageStateEntry entry = GetPageStateEntry(pageName);
            if (entry != null)
            {
                mPageEntries.Remove(pageName);
            }
        }
    }
}
