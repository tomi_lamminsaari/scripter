using Scripter.Controls;
using Scripter.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Scripter.View
{
    public sealed partial class ScenesView : UserControl, INotifyPropertyChanged, ICustomSplitViewContent
    {

        public class OpenSceneCommand : ICommand
        {
            public event EventHandler CanExecuteChanged;

            public bool CanExecute(object parameter)
            {
                return true;
            }

            public void Execute(object parameter)
            {
                Debug.WriteLine("List item clicked!");
            }
        };

        /// <summary>
        /// Key in extra view parameters dictionary that indicates which scene should
        /// be scrolled visible. 
        /// </summary>
        public const string SCROLL_TO_SCENE = "scroll-to-scene";

        /// <summary>
        /// A key that is used in PageStateStorage to store the scene item that held
        /// selection in scenes list when navigated away from this view.
        /// </summary>
        public const string PAGESTATESTORAGE_SELECTED_SCENE_INDEX = "selectedSceneIndex";

        /*
            Member variables
        */
        private IParentPageHelper mHelper;
        private ObservableCollection<CompactSceneItem> mQuickLinkItems = new ObservableCollection<CompactSceneItem>();
        private DataModel.Converters.ScriptExporter mExporter;
        private DispatcherTimer dispatchTimer = new DispatcherTimer();
        private int mContextMenuItemIndex = -1;

        /*
            Binded properties
        */
        public ObservableCollection<CompactSceneItem> ScenesQuickLinksList
        {
            get
            {
                return mQuickLinkItems;
            }
        }

        public string ScriptTitleText
        {
            get { return mHelper.GetActiveDocument().GetScriptTitle(); }
            set { mHelper.GetActiveDocument().SetScriptTitle(value); }
        }

        public ObservableCollection<DataModel.Scene> SceneListItemsSource
        {
            get { return mHelper.GetActiveDocument().GetSceneList(); }
        }

        public DateTimeOffset ScriptDate
        {
            get
            {
                var doc = mHelper.GetActiveDocument();
                if (doc != null)
                {
                    return new DateTimeOffset(doc.ScriptDate);
                }
                return DateTime.Today;
            }
            set
            {
                var doc = mHelper.GetActiveDocument();
                if (doc != null)
                {
                    doc.ScriptDate = value.DateTime;
                }
            }
        }


        /*
            Methods
        */
        public ScenesView(IParentPageHelper helper)
        {
            this.InitializeComponent();
            this.mHelper = helper;

            this.DataContext = this;
            this.ScenesList.KeyDown += ScenesList_KeyDown;
            this.SceneHeaderQuickEditTextBox.KeyDown += SceneHeaderQuickEditTextBox_KeyDown;

            this.dispatchTimer.Tick += DispatchTimer_Tick;
        }


        /// <summary>
        /// Event handler for processing the left click of scene item in
        /// quick links list on the left.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScenesQuickLinkItem_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Debug.WriteLine("ListBox element: LEFT CLICK!");

            var fwObj = sender as FrameworkElement;
            if (fwObj != null)
            {
                var dataContext = fwObj.DataContext as CompactSceneItem;
                int sceneIndex = dataContext.SceneIndex;
                if (sceneIndex >= 0 && sceneIndex < this.SceneListItemsSource.Count)
                {
                    // The ListBoxItem seem to capture the tap events and they don't propagate
                    // to the underlying listbox. We handle the item clicking by
                    // changing the selected item from underlying ListBox. This change of
                    // selected item index will automatically call the SelectionChanged-
                    // event handler of the ListBox which will be handled in
                    // ScenesQuickLinks_SelectionChanged-method below.
                    Debug.WriteLine("    Clicked scene index: {0}", sceneIndex);
                    this.ScenesQuickLinks.SelectedIndex = sceneIndex;
                    e.Handled = true;
                }
                else
                {
                    Debug.WriteLine("ERR: Clicked scene index was invalid!");
                }
            }
            
        }

        /// <summary>
        /// Event handler for processing the right click of list item in
        /// scenes quick links list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScenesQuickLinkItem_RightTapped(object sender, RightTappedRoutedEventArgs e)
        {
            Debug.WriteLine("ListBoxItem element: RIGHT CLICK!");
            var fwObj = sender as FrameworkElement;
            if (fwObj != null)
            {
                var dataContext = fwObj.DataContext as CompactSceneItem;
                var flyout = FlyoutBase.GetAttachedFlyout(fwObj);
                if (dataContext != null && flyout != null)
                {
                    var doc = mHelper.GetActiveDocument();
                    if (dataContext.SceneIndex < doc.ScenesList.Count)
                    {
                        // Show the context menu.
                        // If right clicked on last scene list item, then we hide the
                        // "add below" action.
                        MenuFlyoutItem addBelowItem = fwObj.FindName("AddAfterMenuAction") as MenuFlyoutItem;
                        addBelowItem.Visibility = (dataContext.SceneIndex == doc.ScenesList.Count - 1) ? Visibility.Collapsed : Visibility.Visible;
                        mContextMenuItemIndex = dataContext.SceneIndex;
                        flyout.ShowAt(fwObj);
                        
                    }
                    Debug.WriteLine("dataContext Scene Index: " + dataContext.SceneIndex.ToString());
                }
                else
                {
                    Debug.WriteLine("DataContext or flyout is NULL!");
                }
            }
        }
        

        private void AddAboveMenuAction_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("AddAboveMenuAction_Click(): Add new scene after!");
            Debug.WriteLine("    sender type: " + sender.ToString());
            //var flyoutMenuItem = sender as MenuFlyoutItem;

            var frame = (Frame)Window.Current.Content;
            IScenePanelObserver obs = (IScenePanelObserver)frame.Content;
            var doc = mHelper.GetActiveDocument();
            var clickedScene = doc.GetSceneList().ElementAt(mContextMenuItemIndex);
            if (clickedScene != null)
            {
                obs.CreateNewScene(clickedScene, SceneInsertionPos.BeforeRefScene);
            }
        }

        private void AddAfterMenuAction_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("AddAfterMenuAction_Click(): Add new scene after!");
            Debug.WriteLine("    sender type: " + sender.ToString());
            //var flyoutMenuItem = sender as MenuFlyoutItem;

            var panelObserver = mHelper.GetCurrentPageFrame().Content as IScenePanelObserver;
            var doc = mHelper.GetActiveDocument();
            var clickedScene = doc.GetSceneList().ElementAt(mContextMenuItemIndex);
            if (clickedScene != null)
            {
                panelObserver.CreateNewScene(clickedScene, SceneInsertionPos.AfterRefScene);
            }


        }

        private void DispatchTimer_Tick(object sender, object e)
        {
            Debug.WriteLine("Dispatch timer !!!");
            this.dispatchTimer.Stop();
            this.ScenesList.ScrollIntoView(this.ScenesList.SelectedItem);
            this.ScenesQuickLinks.SelectedIndex = this.ScenesList.SelectedIndex;
            this.ScenesQuickLinks.ScrollIntoView(this.ScenesQuickLinks.SelectedItem);
        }

        public void RefreshScenesQuickLinkList()
        {
            mQuickLinkItems.Clear();
            var doc = mHelper.GetActiveDocument();
            if (doc != null)
            {
                int index = 0;
                foreach (var scene in doc.GetSceneList())
                {
                    var item = new CompactSceneItem();
                    item.SceneIndex = index++;
                    item.SceneNumber = scene.GetSceneNumber().ToString();
                    item.IntExt = scene.EnvIndicator;
                    item.SceneTitle = scene.Heading;
                    mQuickLinkItems.Add(item);
                }
            }
            this.ScenesQuickLinks.Padding = new Thickness(0.0);
            NotifyPropertyChanged("ScenesQuickLinksList");
        }


        public void SetSelectedSceneIndex(int sceneIndex)
        {
            this.ScenesList.SelectedIndex = sceneIndex;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        /// <summary>
        /// Implementation of ICustomSplitViewContent.RefreshCustomContent().
        /// </summary>
        /// <param name="newPageParams"></param>
        public void RefreshCustomContent(Dictionary<string, object> newPageParams)
        {
            RefreshScenesQuickLinkList();
            if (newPageParams != null)
            {
                object sceneObjToShow = null;
                newPageParams.TryGetValue(ScenesView.SCROLL_TO_SCENE, out sceneObjToShow);

                // Find the index of scene to show and set it selected.
                Scene sceneToShow = sceneObjToShow as Scene;
                int sceneIndex = this.SceneListItemsSource.IndexOf(sceneToShow);
                if (sceneIndex >= 0)
                {
                    this.SetSelectedSceneIndex(sceneIndex);
                    this.dispatchTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
                    this.dispatchTimer.Start();
                }
            }
        }

        public void HandleNavigateFromEvent(PageStateEntry entry, Windows.UI.Xaml.Navigation.NavigationEventArgs args)
        {
            this.dispatchTimer.Stop();
            int index = this.ScenesList.SelectedIndex;
            entry.SetValue(PAGESTATESTORAGE_SELECTED_SCENE_INDEX, index);
        }

        public void HandleNavigateToEvent(PageStateEntry entry, Windows.UI.Xaml.Navigation.NavigationEventArgs args)
        {
            if (entry != null)
            {
                int index = entry.GetIntValue(PAGESTATESTORAGE_SELECTED_SCENE_INDEX, -1);
                this.ScenesList.SelectedIndex = index;
                
                // There was a problem to get listview scrolled to correct item
                // by setting it in OnNavigateTo function. That's why we use
                // DispatchTimer to scroll it to selected item after a short
                // delay.
                this.dispatchTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
                this.dispatchTimer.Start();
                this.NotifyPropertyChanged("ScenesList");
            }
        }


        /*
            Event handlers
        */
        private void SceneList_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            Debug.WriteLine("Double clicked!");

            var doc = mHelper.GetActiveDocument();
            int index = this.ScenesList.SelectedIndex;
            Scene scene = doc.GetSceneList().ElementAt(index);
            IScenePanelObserver obs = (IScenePanelObserver)mHelper.GetCurrentPageFrame().Content;
            obs.OpenSceneEditor(scene);
        }

        private void SceneList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Debug.WriteLine("SceneList_SelectionChanged()!");
        }

        private void ScenesQuickLinks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Debug.WriteLine("ScenesView.xaml.cs: Selection changed.!");
            int index = this.ScenesQuickLinks.SelectedIndex;
            this.ScenesList.SelectedIndex = index;
            this.ScenesList.ScrollIntoView(this.ScenesList.SelectedItem);
        }

        private void ScriptPropertiesButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.ScriptPropertiesPopup.IsOpen)
            {
                this.ScriptPropertiesPopup.IsOpen = false;
            }
            else
            {
                this.ScriptPropertiesPopup.IsOpen = true;
            }
        }

        private void ScenesList_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            
            Debug.WriteLine("ScenesList_KeyDown pressed!");
            // Key down in ScenesList means the user wants to quick edit the
            // scene heading.
            int index = this.ScenesList.SelectedIndex;
            int totalCount = this.ScenesList.Items.Count;

            // There should be always at least 1 scene, The End scene.
            if (totalCount > 1)
            {
                if (index >= 0 && index < totalCount - 1)
                {
                    // Other than "The End" scene was selected so user can edit the
                    // selected scene. Copy the text from scene object to the
                    // text editor and show it.
                    DataModel.Scene scene = SceneListItemsSource.ElementAt(index);
                    this.SceneHeaderQuickEditTextBox.Text = scene.CombinedHeaderText;

                    ListBox lb = sender as ListBox;
                    //double w = lb.ActualWidth;
                    //double h = lb.ActualHeight;
                    
                    this.SceneHeaderQuickEditPopup.IsOpen = true;
                    this.SceneHeaderQuickEditTextBox.Focus(FocusState.Keyboard);
                    e.Handled = true;
                }
            }
        }

        private async void SaveScriptButton_Click(object sender, RoutedEventArgs e)
        {
            // Show wait animation.
            mHelper.ToggleWaitingAnimation(true);

            // Save document asynchronously.
            mExporter = DataModel.Converters.ScriptExporter.CreateExporter("fountain");
            var scriptDoc = mHelper.GetActiveDocument();

            string baseFileName = scriptDoc.ScriptId + ".fountain";
            Windows.Storage.StorageFile saveFile = await Utils.FileUtilities.OpenPrivateScriptSaveFile(
                    scriptDoc.ScriptId, ".fountain");
            //scriptDoc.SetTargetFileName(saveFile.Path);
            using (Windows.Storage.Streams.IRandomAccessStream saveStream = await saveFile.OpenAsync(Windows.Storage.FileAccessMode.ReadWrite))
            {
                using (Windows.Storage.Streams.DataWriter writer = new Windows.Storage.Streams.DataWriter(saveStream))
                {
                    await mExporter.Export(scriptDoc, writer);
                }
            }


            mExporter = null;

            // Hide wait animation.
            mHelper.ToggleWaitingAnimation(false);
        }

        private async void SceneHeaderQuickEditTextBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                e.Handled = true;

                // Find the currently selected scene object and set the text from
                // textbox to that scene's heading text.
                DataModel.Scene scene = null;
                int selIndex = this.ScenesList.SelectedIndex;
                if (selIndex >= 0)
                {
                    scene = SceneListItemsSource.ElementAt(selIndex);
                }
                if (scene != null)
                {
                    // Parse the text the user entered in quick edit popup.
                    // If format is proper, set the scene heading texts
                    // to actual scene object.
                    var parser = new ScriptTextRecognizer();
                    bool err = false;
                    var dict = parser.GetParsedSceneHeader(this.SceneHeaderQuickEditTextBox.Text, out err);
                    if (dict.Count > 0 && err == false)
                    {
                        string intExt = "";
                        string loc = "";
                        string tod = "";
                        dict.TryGetValue(ScriptTextRecognizer.SceneHeaderPartIntExt, out intExt);
                        dict.TryGetValue(ScriptTextRecognizer.SceneHeaderPartLocation, out loc);
                        dict.TryGetValue(ScriptTextRecognizer.SceneHeaderPartTimeOfDay, out tod);
                        Debug.WriteLine("intExt: {0}, LOC: {1}, TOD: {2}", intExt, loc, tod);
                        scene.SetHeadingText(intExt, loc, tod);

                        int num = scene.GetSceneNumber();
                        var quickItem = mQuickLinkItems.ElementAt(num - 1);
                        quickItem.IntExt = scene.EnvIndicator;
                        quickItem.SceneTitle = scene.Heading;
                        NotifyPropertyChanged("ScenesQuickLinkList");

                        this.SceneHeaderQuickEditTextBox.Text = "";
                        this.SceneHeaderQuickEditPopup.IsOpen = false;
                    }
                    else
                    {
                        // Parse error.
                        var dialog = new MessageDialog("Expected format is as follows: INT. CENTRAL PARK - MORNING");
                        dialog.Title = "Malformed scene heading text";
                        await dialog.ShowAsync();
                        this.SceneHeaderQuickEditPopup.IsOpen = true;
                        this.SceneHeaderQuickEditTextBox.Focus(FocusState.Keyboard);
                    }
                }
            }
        }
    }
}
