﻿using Scripter.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter.ViewModel
{
    class HomeViewModel : INotifyPropertyChanged
    {
        private HomeView mOwner;
        private List<string> mRecentScriptIds = new List<string>();
        private ObservableCollection<RecentDocumentListItem> mRecentDocumentsList = new ObservableCollection<RecentDocumentListItem>();


        public ObservableCollection<RecentDocumentListItem> RecentDocumentFiles
        {
            get { return mRecentDocumentsList; }
        }


        public void AddToRecentDocuments(string scriptId, string scriptTitle)
        {
            RecentDocumentsList.AddRecentDocument(scriptId, scriptTitle);
        }


        public HomeViewModel(HomeView owningView)
        {
            mOwner = owningView;
        }


        public void RefreshRecentDocuments()
        {
            if (mRecentDocumentsList.Count == 0)
            {
                // TODO: Remove these test items.
                mRecentDocumentsList.Add(new RecentDocumentListItem("ncdjndcjn", "Penan leffa"));
                mRecentDocumentsList.Add(new RecentDocumentListItem("kdjfjjfkf", "Jorman leffa"));
                mRecentDocumentsList.Add(new RecentDocumentListItem("kdifkgkgk", "Janin leffa"));
            }
            NotifyPropertyChanged("RecentDocumentFiles");
        }

        public void UpdateScriptsList(List<string> scripts)
        {
            mRecentDocumentsList.Clear();
            mRecentDocumentsList.Add(new RecentDocumentListItem("Lalaa", "Callback update!"));
            NotifyPropertyChanged("RecentDocumentFiles");
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
