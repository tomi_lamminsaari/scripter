﻿using Scripter.Controls;
using Scripter.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Scripter.ViewModel
{
    /// <summary>
    /// The view model class of Scene editing view.
    /// </summary>
    class SceneEditingViewModel : INotifyPropertyChanged
    {

        Scene mScene;

        /// <summary>
        /// A property that returns the INT. or EXT. information of
        /// this scene.
        /// </summary>
        public string EnvIndicator
        {
            get
            {
                if (mScene == null)
                {
                    return "";
                }
                else
                {
                    return mScene.EnvIndicator;
                }
            }
        }


        /// <summary>
        /// A property where the scene heading is stored to.
        /// </summary>
        public string Heading
        {
            get
            {
                if (mScene == null)
                {
                    return "";
                }
                else
                {
                    return mScene.Heading;
                }
            }
        }


        /// <summary>
        /// A string property that contains the body text of this
        /// scene.
        /// </summary>
        public string SceneBodyText
        {
            get
            {
                if (mScene == null)
                {
                    return "";
                }
                else
                {

                    return FormatSceneContents();
                }
            }
        }


        /// <summary>
        /// Constructor.
        /// </summary>
        public SceneEditingViewModel()
        {

        }


        /// <summary>
        /// Sets the reference to the scene object that is being edited
        /// in this view.
        /// </summary>
        /// <param name="scene">Reference to the scene object that will be
        /// edited.</param>
        public void SetEditedScene(Scene scene)
        {
            mScene = scene;
            NotifyPropertyChanged("");
        }


        /// <summary>
        /// Returns the reference to Scene object that is being edited
        /// in this view currently.
        /// </summary>
        /// <returns>The Scene object being edited.</returns>
        public Scene GetEditedScene()
        {
            return mScene;
        }


        /// <summary>
        /// Formats the scene's textual contents so that they can be
        /// showed inside a textbox for editing.
        /// </summary>
        /// <returns>The string that contains the scene contents in
        /// textual format.</returns>
        private string FormatSceneContents()
        {
            StringBuilder sb = new StringBuilder();
            List<SceneElement> sceneElems = mScene.GetSceneElements();
            foreach (SceneElement elem in sceneElems)
            {
                if (elem.GetType() == typeof(DataModel.SceneElementAction))
                {
                    var actionObj = (DataModel.SceneElementAction)elem;
                    sb.Append(actionObj.GetAsString());
                    sb.Append("\n\n");
                }
                else if (elem.GetType() == typeof(DataModel.SceneElementDialog))
                {
                    var dq = (DataModel.SceneElementDialog)elem;
                    sb.Append(dq.GetAsString());
                    sb.Append("\n\n");
                }
            }
            return sb.ToString();
        }


        /// <summary>
        /// Takes the edited scene contents from UI and updates the
        /// current scene object so that this content is stored to it.
        /// </summary>
        /// <param name="editors">Array of scene editor ui elements that
        /// contain the user edited scene element contents.</param>
        public void SaveEditedContentToScene(List<SceneElementEditor> editors)
        {
            mScene.ClearSceneElements();
            foreach (SceneElementEditor ed in editors)
            {
                // Copy the contents from editors to the DataModel.SceneElement
                // objects.
                ed.UpdateEditorContentToSceneElement();

                // Add the DataModel.SceneElement object to the DataModel.Scene
                // object.
                mScene.AddSceneElement(ed.GetSceneElement());
            }

            // Now the mScene instance has all the scene related stuff from editors.
            // Refresh the index.
            //mScene.GetDocument().RefreshIndex();
        }



        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
